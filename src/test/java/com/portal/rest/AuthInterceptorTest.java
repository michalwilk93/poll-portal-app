package com.portal.rest;

import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.impl.CurrentUserService;
import org.junit.Test;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static com.googlecode.catchexception.CatchException.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
//authentication //TODO
public class AuthInterceptorTest {

    //
    @Test
    public void shouldThrowAuthException() throws Exception {
        //given
        AuthInterceptor authInterceptor = new AuthInterceptor(null);
        HttpServletRequest request = mock(HttpServletRequest.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        when(request.getCookies()).thenReturn(new Cookie[]{});
        //when
        catchException(authInterceptor).preHandle(request, response, null);
        //then
        assertTrue(caughtException() instanceof AuthException);
        AuthException authException = caughtException();
        assertEquals("No token", authException.getErrorMessage());
        assertEquals(103, authException.getErrorCode());
    }


    @Test
    public void shouldCallCurrentUserService() throws Exception {
        //given
        CurrentUserService currentUserService =  mock(CurrentUserService.class);
        AuthInterceptor authInterceptor = new AuthInterceptor(currentUserService );
        HttpServletRequest request = mock(HttpServletRequest.class);
        when(request.getRemoteAddr()).thenReturn("ADDR_VAL");
        HttpServletResponse response = mock(HttpServletResponse.class);
        Cookie c = new Cookie("X-Access-Token","TOKEN_VAL");
        when(request.getCookies()).thenReturn(new Cookie[]{c});
        //when
        authInterceptor.preHandle(request, response, null);
        //then
        verify(currentUserService).identifyCurrentUser("TOKEN_VAL","ADDR_VAL");

    }
}
