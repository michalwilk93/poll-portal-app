package com.portal.domain.api.types;

import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

public class ComparisonMatrixTest {
    @Test
    public void constructorShouldThrowNPE() {
        try {
            //when
            ComparisonMatrix comparisonMatrix = new ComparisonMatrix(null);
            fail();
        } catch (Exception e) {
            //then
            assertTrue(e instanceof NullPointerException);
        }
    }

    @Test
    public void constructorShouldThrowIAEBadMatrixSize() {
        try {
            //when
            ComparisonMatrix comparisonMatrix = new ComparisonMatrix(new double[][]{});
            fail();
        } catch (Exception e) {
            //then
            assertTrue(e instanceof IllegalArgumentException);
            assertEquals("Bad constructor argument", e.getMessage());
        }
    }

    @Test
    public void constructorOk() {
        try {
            //given
            double[][] arg = new double[][]{{1, 2}, {2, 1}};
            //when
            ComparisonMatrix comparisonMatrix = new ComparisonMatrix(arg);
            //then
            assertTrue(comparisonMatrix.getSize() == arg.length);
        } catch (Exception e) {
            fail();
        }
    }

    @Test
    public void convertOk(){
        //given
        double[][] arg = new double[][]{{1.0, 2.0}, {0.5, 1.0}};
        ComparisonMatrix comparisonMatrix = new ComparisonMatrix(arg);
        //when
        double[][]  converterd = comparisonMatrix.asDoubleMatrix();
        ///then
        for (int i = 0; i < arg.length; i++) {
            for (int j = 0; j < arg.length; j++) {
                assertEquals(arg[i][j], converterd[i][j],0.0001);
            }
        }
    }
    /*
    @Test
    public void getMaxEigenvalueOk(){
        //given
        double[][] arg = new double[][]{{1.0, 2.0}, {0.5, 1.0}};
        //when
        ComparisonMatrix comparisonMatrix = new ComparisonMatrix(arg);
        //then
        assertEquals(comparisonMatrix.getMaxEigenvalue(),2.0,0.001);
    }

    * */

}
