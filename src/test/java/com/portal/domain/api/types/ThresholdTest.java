package com.portal.domain.api.types;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junitparams.JUnitParamsRunner.$;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
@RunWith(JUnitParamsRunner.class)
public class ThresholdTest {
    private static final Object[] geBadThresholdValues(){
        return $(new Double(1.0),new Double(0.0),new Double(2.0),new Double(-2.0));
    }

    @Test
    @Parameters(method = "geBadThresholdValues")
    public void constructorShouldThrowIAEBadValue(Double badVal) {
        try {
            //when
            Threshold threshold = new Threshold(badVal.doubleValue());
            fail();
        } catch (Exception e) {
            //then
            assertTrue(e instanceof IllegalArgumentException);
            assertEquals("Threshold must lie between 0 and 1 exclusively", e.getMessage());
        }
    }
}
