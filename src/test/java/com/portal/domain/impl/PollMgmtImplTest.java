package com.portal.domain.impl;


import com.portal.db.daos.JudgementMethodDaoImpl;
import com.portal.db.daos.PollDaoImpl;
import com.portal.domain.api.FilledPoll;
import com.portal.domain.api.MathEngine;
import com.portal.domain.api.exceptions.PollException;
import com.portal.domain.api.types.Id;
import com.portal.domain.spi.JudgementMethodDao;
import com.portal.domain.spi.PollDao;
import com.portal.domain.spi.StatisticsDao;

import static com.googlecode.catchexception.CatchException.catchException;
import static com.googlecode.catchexception.CatchException.caughtException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Test;

import java.util.UUID;

public class PollMgmtImplTest {
    @Test
    public void noSuchJudgementMethodException() throws PollException {
        //given
        MathEngine me = mock(MathEngineImpl.class);
        PollDao pollDao = mock(PollDaoImpl.class);
        CurrentUserService currentUserService = mock(CurrentUserService.class);
        StatisticsDao statisticsDao = mock(StatisticsDao.class);
        FilledPoll fp = mock(FilledPoll.class);
        Id id = new Id(UUID.randomUUID().toString());
        Instantiator<JudgementMethod> instantiator = mock(Instantiator.class);
        when(fp.getJudgementMethodId()).thenReturn(id);
        JudgementMethodDao judgementMethodDao = mock(JudgementMethodDaoImpl.class);
        when(judgementMethodDao.getJudgementMethod(id)).thenReturn(null);
        PollMgmtImpl pollMgmt = new PollMgmtImpl(me, pollDao, currentUserService, statisticsDao, judgementMethodDao,instantiator);
        //when
        catchException(pollMgmt.createFeedBack(fp, null));
        //then
        assertTrue(caughtException() instanceof PollException);
        PollException pollException = caughtException();
        assertEquals("Chosen judgement method is now not avaiable.", pollException.getErrorMessage());
        assertEquals(510, pollException.getErrorCode());
    }
}
