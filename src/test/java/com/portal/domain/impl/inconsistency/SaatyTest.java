package com.portal.domain.impl.inconsistency;


import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Threshold;


import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junitparams.JUnitParamsRunner.$;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class SaatyTest {

    private static final Object[] getConsistentMatrices() {
        Threshold t = new Threshold(0.1);
        return $(
                $(new ComparisonMatrix(new double[][]{
                        {1.0, 3.0, 3.0},
                        {0.3333333333333333, 1.0, 1.0},
                        {0.3333333333333333, 1.0, 1.0}}),
                        t),
                $(new ComparisonMatrix(new double[][]{
                        {1.0, 2.0, 2.0},
                        {0.5, 1.0, 1.0},
                        {0.5, 1.0, 1.0}}),
                        t),
                $(new ComparisonMatrix(new double[][]{
                                {1.0, 1.0, 1.0},
                                {1.0, 1.0, 1.0},
                                {1.0, 1.0, 1.0}}),
                        t),
                $(new ComparisonMatrix(new double[][]{
                                {1.0, 1.0},
                                {1.0, 1.0}}),
                        t)
        );
    }

    @Test
    @Parameters(method = "getConsistentMatrices")
    public void shouldBeConsistent(ComparisonMatrix matrix, Threshold threshold) {
        //given
        Saaty saaty = new Saaty();
        //when
        boolean result = saaty.isConsistent( matrix,threshold);
        //then
        assertTrue(result);
    }


    @Test
    @Parameters(method = "getInConsistentMatrices")
    public void shouldNotBeConsistent(ComparisonMatrix matrix, Threshold threshold) {
        //given
        Saaty saaty = new Saaty();
        //when
        boolean result = saaty.isConsistent( matrix,threshold);
        //then
        assertFalse(result);
    }

    private static final Object[] getInConsistentMatrices() {
        Threshold t = new Threshold(0.01);
        return $(
                $(new ComparisonMatrix(new double[][]{
                                {1.0, 3.0, 3.0},
                                {4.0, 1.0, 1.0},
                                {13.0, 1.0, 1.0}}),
                        t),
                $(new ComparisonMatrix(new double[][]{
                                {1.0, 9.0, 9.0},
                                {123.5, 1.0, 9.0},
                                {999.0, 1.0, 1.0}}),
                        t),
                $(new ComparisonMatrix(new double[][]{
                                {1.0, 1.0, 1.0},
                                {10.0, 1.0, 1000.0},
                                {90.0, 1.0, 1.0}}),
                        t)
        );
    }


}
