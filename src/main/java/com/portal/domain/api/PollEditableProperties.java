package com.portal.domain.api;

import com.portal.domain.api.types.*;

public class PollEditableProperties {

    private final String name;
    private final String description;
    private final Inconsistency inconsistencyMethod;
    private final GradeScale scale;
    private final Threshold inconsistencyThreshold;
    private final Id judgementMethodId;
    private final Access access;

    public PollEditableProperties(String name,
                                  String description,
                                  Inconsistency inconsistencyMethod,
                                  GradeScale scale,
                                  Threshold inconsistencyThreshold,
                                  Id judgementMethodId,
                                  Access access) {
        this.name = name;
        this.description = description;
        this.inconsistencyMethod = inconsistencyMethod;
        this.scale = scale;
        this.inconsistencyThreshold = inconsistencyThreshold;
        this.judgementMethodId = judgementMethodId;
        this.access = access;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Inconsistency getInconsistencyMethod() {
        return inconsistencyMethod;
    }

    public GradeScale getScale() {
        return scale;
    }

    public Threshold getInconsistencyThreshold() {
        return inconsistencyThreshold;
    }

    public Id getJudgementMethodId() {
        return judgementMethodId;
    }

    public Access getAccess() {
        return access;
    }
}
