package com.portal.domain.api.types;


public class UserCredentials {
    private final String name;
    private final String password;
    private final AccountStatus status;

    public UserCredentials(String name, String password, AccountStatus status) {
        this.name = name;
        this.password = password;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public AccountStatus getStatus() {
        return status;
    }
}
