package com.portal.domain.api.types;


public class SignInRequest {
    private final String ip;
    private final String name;
    private final String password;

    public SignInRequest(String ip, String name, String password) {
        this.ip = ip;
        this.name = name;
        this.password = password;
    }

    public String getIp() {
        return ip;
    }

    public String getPassword() {
        return password;
    }

    public String getName() {
        return name;
    }
}
