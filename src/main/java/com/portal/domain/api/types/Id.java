package com.portal.domain.api.types;

import java.util.UUID;

public class Id {
    private final UUID uuid;

    public Id(String uuid) {
        this.uuid = UUID.fromString(uuid);
    }

    public String toString() {
        return uuid.toString();
    }
}
