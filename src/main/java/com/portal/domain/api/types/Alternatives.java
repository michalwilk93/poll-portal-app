package com.portal.domain.api.types;

import java.util.List;

public class Alternatives {
    private final List<String> alternatives;

    public Alternatives(List<String> alternatives) {
        this.alternatives = alternatives;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }
}
