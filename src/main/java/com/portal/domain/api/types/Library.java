package com.portal.domain.api.types;


public class Library {
    private final Id id;
    private final String fileName;
    private final byte[] bytes;

    public Library(Id id, String fileName, byte[] bytes) {
        this.id = id;
        this.fileName = fileName;
        this.bytes = bytes;
    }

    public Id getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public byte[] getBytes() {
        return bytes;
    }
}
