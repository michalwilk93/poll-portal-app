package com.portal.domain.api.types;


public class GradeScale {
    private final double min;
    private final double max;
    private final double interval;

    public GradeScale(double min, double max, double interval) {
        this.min = min;
        this.max = max;
        this.interval = interval;
    }

    public double getMin() {
        return min;
    }

    public double getMax() {
        return max;
    }

    public double getInterval() {
        return interval;
    }
}
