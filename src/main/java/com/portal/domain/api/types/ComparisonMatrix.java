package com.portal.domain.api.types;
/*
import org.jblas.ComplexDouble;
import org.jblas.ComplexDoubleMatrix;
import org.jblas.DoubleMatrix;
import org.jblas.Eigen;


*/
import java.io.Serializable;
public class ComparisonMatrix implements Serializable{
    private final double[][] matrix;
    private final int size;

    public ComparisonMatrix(double[][] matrix) {
        if (matrix.length < 2) {
            throw new IllegalArgumentException("Bad constructor argument");
        }

        this.matrix = matrix;
        size = matrix.length;
    }

    public double getElement(int row, int col) {
        if (col == row) {
            return 1d;
        } else if (col < row) {
            return matrix[row][col];
        } else {
            return 1 / matrix[col][row];
        }
    }

    public int getSize() {
        return size;
    }
/*
    public double getMaxEigenvalue() {
        DoubleMatrix matrix = new DoubleMatrix(asDoubleMatrix());
        ComplexDoubleMatrix eigenvalues = Eigen.eigenvalues(matrix);
        ComplexDouble[] cdArr = eigenvalues.toArray();
        double result = cdArr[0].abs();
        for (ComplexDouble eigenvalue : cdArr) {
            if (eigenvalue.abs() > result) {
                result = eigenvalue.abs();
            }
        }
        return result;
    }*/

    public double[][] asDoubleMatrix() {
        final int size = this.getSize();
        double[][] arr = new double[size][size];
        for (int r = 0; r < size; r++) {
            for (int c = 0; c < size; c++) {
                arr[r][c] = this.getElement(r, c);
            }
        }
        return arr;
    }
}
