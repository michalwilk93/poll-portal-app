package com.portal.domain.api.types;

public class Threshold {
    private final double value;

    public Threshold(double value) {
        if (value <= 0.0 || value >= 1.0) {
            throw new IllegalArgumentException("Threshold must lie between 0 and 1 exclusively");
        }
        this.value = value;
    }

    public double getValue() {
        return value;
    }

}
