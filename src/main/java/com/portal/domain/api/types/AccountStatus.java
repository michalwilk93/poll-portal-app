package com.portal.domain.api.types;

public enum AccountStatus {
    DISABLED,
    USER,
    ADMIN,
    ROOT;
}
