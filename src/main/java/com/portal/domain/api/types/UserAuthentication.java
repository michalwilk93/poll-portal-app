package com.portal.domain.api.types;

public class UserAuthentication{
    private final AccountStatus status;
    private final String userName;

    public UserAuthentication(AccountStatus status, String userName) {
        this.status = status;
        this.userName = userName;
    }


    public AccountStatus getStatus() {
        return status;
    }

    public String getUserName() {
        return userName;
    }
}