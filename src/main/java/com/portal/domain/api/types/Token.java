package com.portal.domain.api.types;

import java.sql.Timestamp;

public class Token {
    private final String userName;
    private final String ip;
    private final Timestamp creationTime;
    private final String tokenValue;

    public Token(String userName, String ip, Timestamp creationTime, String tokenValue) {
        this.userName = userName;
        this.ip = ip;
        this.creationTime = creationTime;
        this.tokenValue = tokenValue;
    }

    public String getUserName() {
        return userName;
    }

    public String getIp() {
        return ip;
    }

    public Timestamp getCreationTime() {
        return creationTime;
    }

    public String getTokenValue() {
        return tokenValue;
    }
}
