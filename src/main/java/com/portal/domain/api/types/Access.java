package com.portal.domain.api.types;

public enum Access {
    PUBLIC, PRIVATE
}
