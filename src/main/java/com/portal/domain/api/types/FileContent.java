package com.portal.domain.api.types;

import java.util.List;

public class FileContent {
    private final String code;
    private final String name;
    private final String imports;
    private final List<Id> libsIds;

    public FileContent(String code, String name, String imports, List<Id> libsIds) {
        this.code = code;
        this.name = name;
        this.imports = imports;
        this.libsIds = libsIds;
    }

    public List<Id> getLibsIds() {
        return libsIds;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public String getImports() {
        return imports;
    }
}
