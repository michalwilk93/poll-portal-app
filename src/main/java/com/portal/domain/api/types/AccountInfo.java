package com.portal.domain.api.types;


public class AccountInfo {
    private final  String name;
    private final AccountStatus status;

    public AccountInfo(String name, AccountStatus status) {
        this.name = name;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public boolean isAtLeast(AccountStatus other){
        return status.ordinal() >= other.ordinal();
    }
}
