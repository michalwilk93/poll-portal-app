package com.portal.domain.api.types;

import java.util.List;

public class Node implements java.io.Serializable {
    private String name;
    private List<Node> subcriteria;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Node> getSubcriteria() {
        return subcriteria;
    }

    public void setSubcriteria(List<Node> subcriteria) {
        this.subcriteria = subcriteria;
    }
}
