package com.portal.domain.api.types;

import java.io.Serializable;
import java.util.Map;

public class Descriptions implements Serializable {
    private final Map<String, Map<String, String>> alternativesDescriptions;

    public Map<String, Map<String, String>> getAlternativesDescriptions() {
        return alternativesDescriptions;
    }

    public Descriptions(Map<String, Map<String, String>> alternativesDescriptions) {
        this.alternativesDescriptions = alternativesDescriptions;
    }
}
