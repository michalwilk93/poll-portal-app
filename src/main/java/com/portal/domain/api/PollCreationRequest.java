package com.portal.domain.api;

import com.portal.domain.api.types.*;

public class PollCreationRequest {
    private final PollEditableProperties properties;
    private final Alternatives alternatives;
    private final String author;
    private final Node criteriaTree;
    private final Descriptions alternativesDescriptions;

    public PollCreationRequest(PollEditableProperties properties, Alternatives alternatives,
                               String author, Node criteriaTree, Descriptions alternativesDescriptions) {
        this.properties = properties;
        this.alternatives = alternatives;
        this.author = author;
        this.criteriaTree = criteriaTree;
        this.alternativesDescriptions = alternativesDescriptions;
    }

    public PollEditableProperties getProperties() {
        return properties;
    }

    public Alternatives getAlternatives() {
        return alternatives;
    }

    public String getAuthor() {
        return author;
    }

    public Node getCriteriaTree() {
        return criteriaTree;
    }

    public Descriptions getAlternativesDescriptions() {
        return alternativesDescriptions;
    }


}
