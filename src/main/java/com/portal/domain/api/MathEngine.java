package com.portal.domain.api;

import com.portal.domain.impl.JudgementMethod;

public interface MathEngine {
    public PollResult computePollResult(FilledPoll filledPoll, JudgementMethod jm);

    public boolean checkInconsistency(InconsistencyCheckRequest dataToCheck);
}
