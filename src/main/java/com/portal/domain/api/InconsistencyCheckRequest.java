package com.portal.domain.api;

import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Inconsistency;
import com.portal.domain.api.types.Threshold;

public class InconsistencyCheckRequest {
    private final ComparisonMatrix matrix;
    private final Threshold threshold;
    private final Inconsistency method;

    public InconsistencyCheckRequest(ComparisonMatrix matrix, Threshold threshold, Inconsistency method) {
        this.matrix = matrix;
        this.threshold = threshold;
        this.method = method;
    }

    public Inconsistency getMethod() {
        return method;
    }

    public ComparisonMatrix getMatrix() {
        return matrix;
    }

    public Threshold getThreshold() {
        return threshold;
    }
}

