package com.portal.domain.api;

import com.portal.domain.api.types.Id;
import java.util.List;

public class PollRespondents {
    private final Id pollId;
    private final List<String> respondentsIps;

    public PollRespondents(List<String> respondentsIps, Id pollId) {
        this.respondentsIps = respondentsIps;
        this.pollId = pollId;
    }

    public List<String> getRespondentsIps() {
        return respondentsIps;
    }

    public Id getPollId() {
        return pollId;
    }
}
