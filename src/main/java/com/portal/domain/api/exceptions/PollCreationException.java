package com.portal.domain.api.exceptions;

public class PollCreationException extends PollException  {
    public PollCreationException(String msg, ErrorCodes errorCode) {
        super(msg,errorCode);
    }
}
