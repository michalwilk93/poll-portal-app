package com.portal.domain.api.exceptions;

import java.util.List;

public class PollException extends Exception {
    private final String errorMessage;
    private final ErrorCodes errorCode;
    private List<Object> errorDetails;

    public PollException(String msg, ErrorCodes code) {
        this.errorMessage = msg;
        this.errorCode = code;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public ErrorCodes  getErrorCode() {
        return errorCode;
    }

    public List<Object> getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(List<Object> errorDetails) {
        this.errorDetails = errorDetails;
    }
}
