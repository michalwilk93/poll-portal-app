package com.portal.domain.api.exceptions;

public class IpUsedException extends PollException {
    public IpUsedException(String msg, ErrorCodes  errorCode) {
        super(msg,errorCode);
    }
}