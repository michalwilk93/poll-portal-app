package com.portal.domain.api.exceptions;

public class SignInException extends PollException {
    public SignInException(String msg, ErrorCodes errorCode) {
        super(msg,errorCode);
    }
}