package com.portal.domain.api.exceptions;


public class CompilationException extends PollException {
    public CompilationException(String msg, ErrorCodes  errorCode) {
        super(msg, errorCode);
    }
}
