package com.portal.domain.api.exceptions;

public class AuthException extends PollException {
    public AuthException(String msg, ErrorCodes errorCode) {
        super(msg,errorCode);
    }
}
