package com.portal.domain.api.exceptions;

public class AccountCreationException extends PollException {
    public AccountCreationException(String msg, ErrorCodes errorCode) {
        super(msg,errorCode);
    }
}
