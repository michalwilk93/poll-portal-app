package com.portal.domain.api.exceptions;


public class ClassLoadingException extends PollException {
    public ClassLoadingException(String msg, ErrorCodes errorCode) {
        super(msg,errorCode);
    }
}
