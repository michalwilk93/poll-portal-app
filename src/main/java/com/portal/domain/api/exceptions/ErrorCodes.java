package com.portal.domain.api.exceptions;


public enum ErrorCodes {
    NO_TOKEN(401),
    BAD_CREDENTIALS(402),
    ACCESS_DENIED(403),
    INVALID_TOKEN(405),
    NO_SUCH_METHOD(505),
    IP_ADDRESS_USED(506),
    USER_ALREADY_EXISTS(510),
    RUNTIME_COMPILATION_FAILED(511),
    INSTANTIATION_FAILED(512);

    ErrorCodes(int code) {
        this.codeVal = code;
    }

    private int codeVal;

    public int getCodeVal() {
        return codeVal;
    }
}
