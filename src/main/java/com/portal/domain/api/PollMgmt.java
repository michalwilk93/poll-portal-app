package com.portal.domain.api;

import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.IpUsedException;
import com.portal.domain.api.exceptions.PollCreationException;
import com.portal.domain.api.exceptions.PollException;
import com.portal.domain.api.types.Id;
import java.util.List;

// PollManager
public interface PollMgmt {
    public void createNewPoll(PollCreationRequest newPollPlz) throws PollCreationException;

    public PollDefinition takePoll(Id pollId, String ip) throws IpUsedException;

    public List<PollDefinition> getPublicPolls();

    // to admin service
    public List<PollDefinition> getUserPolls(String userName);
//TODO remove
    public List<PollDefinition> getCurrentUserPolls();

    public void updatePoll(Id id, PollEditableProperties pep) throws PollCreationException;

    public void deletePoll(Id id) throws AuthException;

    public PollResult getGeneralStatistics(Id id);


    public PollResult createFeedBack(FilledPoll fp, String ip) throws PollException;

    public boolean checkInconsistency(InconsistencyCheckRequest dataToCheck);


}
