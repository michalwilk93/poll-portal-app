package com.portal.domain.api;

import com.portal.domain.api.types.*;

public class JudgementMethodDefinition {
    private final Id methodId;
    private final FileContent data;
    private final byte[] compiledFunctionalClass;

    public JudgementMethodDefinition(Id methodId, FileContent data, byte[] compiledFunctionalClass) {
        this.methodId = methodId;
        this.data = data;
        this.compiledFunctionalClass = compiledFunctionalClass;
    }

    public byte[] getCompiledFunctionalClass() {
        return compiledFunctionalClass;
    }

    public Id getMethodId() {
        return methodId;

    }

    public FileContent getData() {
        return data;
    }
}
