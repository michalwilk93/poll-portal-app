package com.portal.domain.api;

import com.portal.domain.api.exceptions.AccountCreationException;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.SignInException;
import com.portal.domain.api.types.*;

import java.util.List;

public interface AccountMgmt {
    public void createAccount(UserCredentials uc) throws AccountCreationException;

    public Token generateToken(SignInRequest sir) throws SignInException;

    public void logout(String tokenVal);

    public AccountStatus getAccountStatus(String un);

    public List<AccountInfo> getAccountsInfo() throws AuthException;

    public void changeAccountStatus(String userName, AccountStatus newStatus) throws AuthException;

    public void deleteAccount(String userName) throws AuthException;
}
