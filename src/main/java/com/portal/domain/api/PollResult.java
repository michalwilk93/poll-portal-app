package com.portal.domain.api;

import com.portal.domain.api.types.Id;
import com.portal.domain.impl.ahp.outputTree.OutputBranch;

public class PollResult {
    private final OutputBranch root;
    private final Id pollId;
    private int respondentCount;

    public PollResult(OutputBranch root,Id pollId) {
        this.root = root;
        this.pollId = pollId;
    }

    public void setRespondentCount(int respondentCount) {
        this.respondentCount = respondentCount;
    }

    public Id getPollId() {
        return pollId;
    }

    public OutputBranch getRoot() {
        return root;
    }

    public int getRespondentCount() {
        return respondentCount;
    }
}
