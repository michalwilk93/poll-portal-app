package com.portal.domain.api;

import com.portal.domain.api.types.Alternatives;
import com.portal.domain.api.types.Descriptions;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.Node;

public class PollDefinition {

    private final Id id;
    private final Alternatives alternatives;
    private final Node criteriaTree;
    private final String author;
    private final Descriptions alternativesDescriptions;

    private PollEditableProperties editableProperties;

    public PollDefinition(
            Id id,
            String author,
            PollEditableProperties properties,
            Alternatives alternatives,
            Node criteriaTree,
            Descriptions alternativesDescriptions) {
        this.id = id;
        this.author = author;
        this.editableProperties = properties;
        this.alternatives = alternatives;
        this.criteriaTree = criteriaTree;
        this.alternativesDescriptions = alternativesDescriptions;
    }

    public void setEditableProperties(PollEditableProperties editableProperties) {
        this.editableProperties = editableProperties;
    }

    public Descriptions getAlternativesDescriptions() {
        return alternativesDescriptions;
    }

    public PollEditableProperties getEditableProperties() {
        return editableProperties;
    }

    public Id getId() {
        return id;
    }

    public String getAuthor() {
        return author;
    }

    public Node getCriteriaTree() {
        return criteriaTree;
    }

    public Alternatives getAlternatives() {
        return alternatives;
    }
}
