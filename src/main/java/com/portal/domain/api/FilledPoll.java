package com.portal.domain.api;

import com.portal.domain.api.types.Id;
import com.portal.domain.impl.ahp.inputTree.InputBranch;
import com.portal.domain.api.types.Inconsistency;

public class FilledPoll {
    private final Id id;
    private final InputBranch root;
    private final Id judgementMethodId;
    private final Inconsistency inconsistencyMethod;

    public FilledPoll(Id id, InputBranch root, Id judgementMethodId, Inconsistency inconsistencyMethod) {
        this.id = id;
        this.root = root;
        this.judgementMethodId = judgementMethodId;
        this.inconsistencyMethod = inconsistencyMethod;
    }

    public Id getJudgementMethodId() {
        return judgementMethodId;
    }

    public Id getId() {
        return id;
    }

    public InputBranch getRoot() {
        return root;
    }

    public Inconsistency getInconsistencyMethod() {
        return inconsistencyMethod;
    }
}

