package com.portal.domain.api;

import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.CompilationException;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Library;

import java.io.IOException;
import java.util.List;

public interface JudgementMethodMgmt {
    public void createJudgementMethod(FileContent judgementMethod) throws AuthException, CompilationException;

    public List<JudgementMethodDefinition> getCustomJudgementMethods() throws AuthException;

    public void editJudgementMethod(Id id,FileContent fileContent) throws AuthException;

    public void addLibrary(byte[] bytes,String fileName) throws IOException, AuthException;

    public List<Library> getLibraries() throws AuthException;
}
