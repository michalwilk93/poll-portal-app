package com.portal.domain.spi;


import com.portal.domain.api.PollRespondents;
import com.portal.domain.api.PollResult;
import com.portal.domain.api.types.Id;

public interface StatisticsDao {
    public void saveStatistics(PollResult pr);

    public PollResult getPollStatistics(Id id);

    public void saveRespondents(PollRespondents pollRespondents);

    public PollRespondents getPollRespondents(Id pollId);

    public void addRespondentIp(Id pollId, String ip);
}
