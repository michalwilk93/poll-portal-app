package com.portal.domain.spi;

import com.portal.domain.api.exceptions.AccountCreationException;
import com.portal.domain.api.types.*;

import java.util.List;

public interface AuthDao {
    public void createAccount(UserCredentials uc) throws AccountCreationException;

    public UserAuthentication identify(String tokenVal, String remoteAddr);

    public boolean credentialsOk(String name, String password);

    public void saveToken(Token t);

    public void deleteToken(String tokenVal);

    public AccountStatus getAccountStatus(String userName);

    public List<AccountInfo> getAccountInfos();

    public void changeAccountStatus(String userName, AccountStatus newStatus);

    public void deleteAccount(String userName) ;
}

