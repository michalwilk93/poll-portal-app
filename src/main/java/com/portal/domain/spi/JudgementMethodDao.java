package com.portal.domain.spi;


import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.exceptions.PollException;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Library;

import java.util.List;

public interface JudgementMethodDao {
    public List<JudgementMethodDefinition> getCustomJudgementMethods();

    public void editJudgementMethod(Id id,FileContent fileContent);

    public JudgementMethodDefinition getJudgementMethod(Id methodId) throws PollException;

    public void saveLibrary(Library library);

    public List<Library> getLibraries();

    public void saveNewJudgementMethod(JudgementMethodDefinition judgementMethodDefinition);

    public List<Library> getLibsWithIds(List<Id> libsIds);
}
