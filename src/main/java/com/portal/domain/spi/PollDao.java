package com.portal.domain.spi;

import com.portal.domain.api.PollEditableProperties;
import com.portal.domain.api.exceptions.PollCreationException;
import com.portal.domain.api.PollDefinition;
import com.portal.domain.api.types.Id;

import java.util.List;

public interface PollDao {
    public PollDefinition getPoll(Id pollId);

    public void deletePoll(Id id);

    public void saveNewPoll(PollDefinition p) throws PollCreationException;

    public List<PollDefinition> getUserPolls(String userName);

    public List<PollDefinition> getPublicPolls();

    public void updateEditableProperties(Id pollId, PollEditableProperties pollEditableProperties);
}