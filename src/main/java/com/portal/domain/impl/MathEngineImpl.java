package com.portal.domain.impl;

import com.portal.domain.api.*;
import com.portal.domain.impl.ahp.inputTree.InputNode;
import com.portal.domain.impl.ahp.outputTree.OutputNode;
import com.portal.domain.impl.ahp.inputTree.InputLeaf;
import com.portal.domain.impl.ahp.inputTree.InputBranch;
import com.portal.domain.impl.ahp.outputTree.OutputLeaf;
import com.portal.domain.impl.ahp.outputTree.OutputBranch;
import com.portal.domain.impl.inconsistency.InconsistencyMethod;
import com.portal.domain.impl.inconsistency.InconsistencyMethodFactory;

import java.util.LinkedList;
import java.util.List;


public class MathEngineImpl implements MathEngine {
    private final InconsistencyMethodFactory factory;

    public MathEngineImpl() {
        this.factory = new InconsistencyMethodFactory();
    }
    @Override
    public boolean checkInconsistency(InconsistencyCheckRequest req) {
        InconsistencyMethod im = factory.getInconsistencyMethod(req.getMethod());
        return im.isConsistent(req.getMatrix(), req.getThreshold());
    }

    @Override
    public PollResult computePollResult(FilledPoll filled, JudgementMethod method) {
        OutputBranch outputTreeRoot = new OutputBranch();
        computeOutputBranch(filled.getRoot(), outputTreeRoot, 1.0, method);
        PollResult result = new PollResult(outputTreeRoot, filled.getId());
        return result;
    }

    private double[] computeOutputLeaf(InputLeaf inLeaf, OutputLeaf outLeaf, double globalWeight, JudgementMethod jm) {
        outLeaf.setName(inLeaf.getName());
        double[] rankingVector = jm.computeWeights(inLeaf.getAlternativePreferences());
        outLeaf.setVector(rankingVector);
        outLeaf.setGlobalWeight(globalWeight);
        return rankingVector.clone();
    }

    private double[] computeOutputBranch(InputBranch inBranch, OutputBranch outBranch, double globalWeight, JudgementMethod jm) {
        double[] subcriteriaWeights = jm.computeWeights(inBranch.getCriteriaPreferences());
        int numberOfChildren = inBranch.getSubcriteria().size();
        double[][] vectorsFromChildren = new double[numberOfChildren][];
        List<OutputNode> newChildren = new LinkedList<>();

        for (int i = 0; i < numberOfChildren; i++) {
            double childWeight = subcriteriaWeights[i] * globalWeight;
            InputNode inChild = inBranch.getSubcriteria().get(i);
            if (inChild instanceof InputBranch) {
                OutputBranch outChild = new OutputBranch();
                newChildren.add(outChild);
                vectorsFromChildren[i] = computeOutputBranch((InputBranch) inChild, outChild, childWeight, jm);
            } else {
                OutputLeaf outChild = new OutputLeaf();
                newChildren.add(outChild);
                vectorsFromChildren[i] = computeOutputLeaf((InputLeaf) inChild, outChild, childWeight, jm);
            }
        }
        outBranch.setSubcriteria(newChildren);
        double[][] multipliedVectors = multiplyEachVectorByHisWeight(vectorsFromChildren, subcriteriaWeights);
        double[] resultVector = forEachAlternativeSumItsWeightsFromVectors(multipliedVectors);
        outBranch.setVector(resultVector);
        return resultVector.clone();
    }

    private double[][] multiplyEachVectorByHisWeight(double[][] arrayOfVectors, double[] vectorWeights) {
        for (int vectorInd = 0; vectorInd < arrayOfVectors.length; vectorInd++) {
            int vectorLength = arrayOfVectors[vectorInd].length;
            for (int k = 0; k < vectorLength; k++) {
                arrayOfVectors[vectorInd][k] *= vectorWeights[vectorInd];
            }
        }
        return arrayOfVectors;
    }

    private double[] forEachAlternativeSumItsWeightsFromVectors(double[][] arrayOfVectors) {
        int numberOfAlternatives = arrayOfVectors[0].length;
        double[] result = new double[numberOfAlternatives];
        for (int altInd = 0; altInd < numberOfAlternatives; altInd++) {
            double sum = 0;
            for (int vecInd = 0; vecInd < arrayOfVectors.length; vecInd++) {
                sum += arrayOfVectors[vecInd][altInd];
            }
            result[altInd] = sum;
        }
        return result;
    }
}