package com.portal.domain.impl;


import com.portal.domain.api.exceptions.CompilationException;
import com.portal.domain.api.exceptions.ErrorCodes;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Library;

import javax.tools.*;
import java.io.*;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//TODO to juz nie jest functional bo user wypelnia tez metode
//kompiluje implementacje jakiegos interfejsu i zwraca byte[] z plikiem .class
public class FunctionalClassCompiler {
    //ta metoda:
    //tworzy temp folder
    //wypakowuje tam jary
    //tworzy tam plik z kodem
    //kompilowac klase ktora implementuje jedna metode i zwracac bajty pliku .class

    public byte[] compileImplementationOf(String interfaceName, FileContent content, List<Library> libs) throws CompilationException {
        Path tmpDir=null;
        try {
            tmpDir = createTmpFolder();
            createAndFillLibsFiles(tmpDir, libs);
            System.out.println("------------------------------------------------------");
            System.out.println(tmpDir.toAbsolutePath());
            List<String> optionList = generateOptionList(tmpDir, libs);
            File compiledFile = createFileWithCode(tmpDir, interfaceName, content);
            DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
            JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
            StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, null);
            Iterable<? extends JavaFileObject> compilationUnit
                    = fileManager.getJavaFileObjectsFromFiles(Arrays.asList(compiledFile));
            JavaCompiler.CompilationTask task = compiler.getTask(
                    null,
                    fileManager,
                    diagnostics,
                    optionList,
                    null,
                    compilationUnit);
            boolean compilationSuccessful = task.call();
            if (compilationSuccessful) {
                return readBytesFromClassFile(tmpDir);
            } else {
                printDiagnostic(diagnostics);
                throw new CompilationException("Cannot compile file.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
            }
        } finally {
            //TODO przyslanianie sie wyjatkow
            deleteTmpDirectory(tmpDir);
        }
    }

    private File createFileWithCode(Path tmpDir, String interfaceName, FileContent content) throws CompilationException {
        File compiledFile = new File(tmpDir.toAbsolutePath() + File.separator + "RuntimeCompiledMethod.java");
        if (compiledFile.getParentFile().exists()) {
            String contentAsString = getFileContentAsOneString(interfaceName, content.getCode(), content.getImports());
            writeContentToFile(compiledFile, contentAsString);
            return compiledFile;
        } else {
            throw new CompilationException("Cannot create compilation file", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }

    private String getFileContentAsOneString(String interfaceName, String classCode, String imports) {
        StringBuilder sb = new StringBuilder();
        sb.append("package runtimeCompilation;\n");
        sb.append(imports);
        sb.append("\npublic class RuntimeCompiledMethod implements " + interfaceName + "{\n");
        sb.append(classCode);
        sb.append("\n}\n");
        return sb.toString();
    }

    private void writeContentToFile(File file, String content) throws CompilationException {
        try (Writer writer = new FileWriter(file)) {
            writer.write(content);
            writer.flush();
        } catch (IOException ex) {
            throw new CompilationException("IO exception during writing file", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }

    private List<String> generateOptionList(Path tmpDir, List<Library> libs) {
        List<String> optionList = new ArrayList<>();
        optionList.add("-classpath");
        StringBuilder libsClasspath = new StringBuilder();
        for (int i = 0; i < libs.size(); i++) {
            libsClasspath.append(";" + tmpDir.toAbsolutePath() + File.separator + libs.get(i).getFileName());
        }
        optionList.add(System.getProperty("java.class.path") + libsClasspath.toString());
        return optionList;
    }

    private void createAndFillLibsFiles(Path tmpDir, List<Library> libraries) throws CompilationException {
        for (Library lib : libraries) {
            try (FileOutputStream fos = new FileOutputStream(tmpDir.toAbsolutePath() + File.separator + lib.getFileName())) {
                fos.write(lib.getBytes());
            } catch (IOException e) {
                throw new CompilationException("Can't write bytes to libs files.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
            }
        }
    }

    private void printDiagnostic(DiagnosticCollector<JavaFileObject> diagnostics) throws CompilationException {
        StringBuilder sb = new StringBuilder();
        for (Diagnostic<? extends JavaFileObject> diagnostic : diagnostics.getDiagnostics()) {
            sb.append(String.format("Error:line %d in %s%n", diagnostic.getLineNumber(), diagnostic.getSource().toUri()));
        }
        System.out.println(sb.toString());
    }

    private void deleteTmpDirectory(Path tmpDir) throws CompilationException {
        try {
            Files.walkFileTree(tmpDir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            throw new CompilationException("Cannot delete temp directory.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }

    private byte[] readBytesFromClassFile(Path tmpDir) throws CompilationException {
        try {
            return Files.readAllBytes(new File(tmpDir.toAbsolutePath() + File.separator + "RuntimeCompiledMethod.class").toPath());
        } catch (IOException e) {
            throw new CompilationException("Cannot read bytes from .class file.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }

    private Path createTmpFolder() throws CompilationException {
        try {
            return Files.createTempDirectory("COMPILATION_");
        } catch (IOException e) {
            throw new CompilationException("Can't create temporary directory.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }
}