package com.portal.domain.impl;

import com.portal.domain.impl.ahp.outputTree.OutputBranch;
import com.portal.domain.impl.ahp.outputTree.OutputLeaf;
import com.portal.domain.impl.ahp.outputTree.OutputNode;


public class StatisticsTreeTraverser {
    public void updateStatisticsTree(OutputNode generalStatNode, OutputNode singleStatNode, int respondents) {
        for (int i = 0; i < generalStatNode.getVector().length; i++) {
            double oldVal = generalStatNode.getVector()[i];
            double newVal = singleStatNode.getVector()[i];
            double weightedVal = (oldVal * respondents + newVal) / (respondents + 1);
            generalStatNode.getVector()[i] = (weightedVal);
        }

        if (generalStatNode instanceof OutputLeaf && singleStatNode instanceof OutputLeaf) {
            OutputLeaf oldLeaf = (OutputLeaf) generalStatNode;
            OutputLeaf newLeaf = (OutputLeaf) singleStatNode;
            double updatedWeight =(oldLeaf.getGlobalWeight() * respondents + newLeaf.getGlobalWeight()/ (respondents + 1));
            oldLeaf.setGlobalWeight(updatedWeight);
        } else if(generalStatNode instanceof OutputBranch && singleStatNode instanceof OutputBranch){
            OutputBranch oldBranch = (OutputBranch) generalStatNode;
            OutputBranch newBranch = (OutputBranch) singleStatNode;
            for (int i = 0; i < oldBranch.getSubcriteria().size(); i++) {
                updateStatisticsTree(oldBranch.getSubcriteria().get(i), newBranch.getSubcriteria().get(i), respondents);
            }
        }
    }
}
