package com.portal.domain.impl;

import com.portal.domain.api.types.ComparisonMatrix;

public interface JudgementMethod {
      double[] computeWeights(ComparisonMatrix m);
}
