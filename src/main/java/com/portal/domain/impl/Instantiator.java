package com.portal.domain.impl;

import com.portal.domain.api.exceptions.*;
import com.portal.domain.api.exceptions.ClassLoadingException;
import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Library;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;

public class Instantiator<T> {

    //metoda ma:
    //wypakowac jary
    //zrobilc plik z bytecodem
    //zaladowac po metodzie klase
    private void createClassFile(Path file, byte[] compiledClass) throws ClassLoadingException {
        try {
            Files.write(file, compiledClass);
        } catch (IOException e) {
            throw new ClassLoadingException("Can't write bytes to .class file.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }


    public T instantiate(Class<T> functionalInterface, byte[] compiledClass, List<Library> libs)
            throws ClassLoadingException {
        Path tmpDir = createTmpFolder();
        System.out.println(tmpDir.toAbsolutePath());
        createAndFillLibsFiles(tmpDir, libs);
        Path classFile = Paths.get(tmpDir.toAbsolutePath() + File.separator + "RuntimeCompiledMethod" + ".class");
        createClassFile(classFile, compiledClass);
        Map<String, byte[]> classes = new HashMap<>();
        classes.put("runtimeCompilation.RuntimeCompiledMethod", compiledClass);
        try {
            URL[] libsUrls = generateLibsUrls(tmpDir, libs);


            ByteClassLoader loader = new ByteClassLoader(libsUrls, classes);
            Class<T> klasa = (Class<T>) loader.findClass("runtimeCompilation.RuntimeCompiledMethod");
            JudgementMethod jm = (JudgementMethod) klasa.newInstance();




            return klasa.newInstance();
        } catch (IllegalAccessException | ClassNotFoundException | InstantiationException ex) {
            throw new ClassLoadingException(ex.getMessage(), ErrorCodes.INSTANTIATION_FAILED);
        } finally {
            //todo
           // deleteTmpDirectory(tmpDir);
        }
    }

    private URL[] generateLibsUrls(Path tmpDir, List<Library> libs) {
        URL[] result = new URL[libs.size()];
        try {
            for (int i = 0; i < libs.size(); i++) {
                result[i] = new File(tmpDir.toAbsolutePath() + File.separator + libs.get(i).getFileName()).toURI().toURL();
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return result;
    }

    private Path createTmpFolder() throws ClassLoadingException {
        try {
            return Files.createTempDirectory("INSTANTIATION_");
        } catch (IOException e) {
            throw new ClassLoadingException("Can't create temporary directory.", ErrorCodes.INSTANTIATION_FAILED);
        }
    }


    private void createAndFillLibsFiles(Path tmpDir, List<Library> libraries) throws ClassLoadingException {
        for (Library lib : libraries) {
            try (FileOutputStream fos = new FileOutputStream(tmpDir.toAbsolutePath() + File.separator + lib.getFileName())) {
                fos.write(lib.getBytes());
            } catch (IOException e) {
                throw new ClassLoadingException("Can't write bytes to libs files.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
            }
        }
    }

    private void deleteTmpDirectory(Path tmpDir) throws ClassLoadingException {
        try {
            Files.walkFileTree(tmpDir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return FileVisitResult.CONTINUE;
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
            throw new ClassLoadingException("Cannot delete temp directory.", ErrorCodes.RUNTIME_COMPILATION_FAILED);
        }
    }

}
