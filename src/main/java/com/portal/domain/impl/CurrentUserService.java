package com.portal.domain.impl;

import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.ErrorCodes;
import com.portal.domain.api.types.AccountStatus;
import com.portal.domain.api.types.UserAuthentication;
import com.portal.domain.spi.AuthDao;
import org.springframework.beans.factory.annotation.Autowired;

public class CurrentUserService {

    private final AuthDao authDao;
    private UserAuthentication userAuthentication;

    @Autowired
    public CurrentUserService(AuthDao authDao) {//user is a proxy for request- scoped bean, so we can inject  it in constructor
        this.authDao = authDao;
    }


    public void identifyCurrentUser(String token, String remoteAddr) throws AuthException {
        UserAuthentication identifiedUser = authDao.identify(token, remoteAddr);
        if (identifiedUser != null) {
            userAuthentication = identifiedUser;
        } else {
            throw new AuthException("Invalid token", ErrorCodes.INVALID_TOKEN);
        }
    }

    public void checkUserIsAdminOrRoot() throws AuthException {
        if(userAuthentication.getStatus() != AccountStatus.ADMIN &&
                userAuthentication.getStatus() != AccountStatus.ROOT) {
            throw new AuthException("You don't have permissions to perform this operation", ErrorCodes.ACCESS_DENIED);
        }
    }

    public void checkUserIsRoot() throws AuthException {
        if(userAuthentication.getStatus() != AccountStatus.ROOT) {
            throw new AuthException("You don't have permissions to perform this operation",ErrorCodes.ACCESS_DENIED);
        }
    }

    public String getUserName() {
        return userAuthentication.getUserName();
    }

    public AccountStatus getStatus(){
        return userAuthentication.getStatus();
    }

}


