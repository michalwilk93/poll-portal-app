package com.portal.domain.impl;

import com.portal.domain.api.exceptions.ErrorCodes;
import com.portal.domain.api.types.*;
import com.portal.domain.api.exceptions.AccountCreationException;
import com.portal.domain.api.AccountMgmt;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.SignInException;
import com.portal.domain.spi.AuthDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class AccountMgmtImpl implements AccountMgmt {

    private final Random random;
    private final AuthDao authDao;

    private CurrentUserService currentUserService;

    @Autowired
    public AccountMgmtImpl(AuthDao authDao, CurrentUserService currentUserService) {
        this.authDao = authDao;
        this.currentUserService = currentUserService;
        this.random = new SecureRandom();
    }

    @Override
    public void createAccount(UserCredentials uc) throws AccountCreationException {
        authDao.createAccount(uc);
    }

    @Override
    public Token generateToken(SignInRequest request) throws SignInException {
        if (authDao.credentialsOk(request.getName(), request.getPassword())) {
            String tokenVal = new BigInteger(130, random).toString(32);
            Date date = new Date();
            Token token = new Token(request.getName(), request.getIp(), new Timestamp(date.getTime()), tokenVal);
            authDao.saveToken(token);
            return token;
        } else {
            throw new SignInException("Bad credentials", ErrorCodes.BAD_CREDENTIALS);
        }
    }

    @Override
    public void logout(String tokenVal) {
        authDao.deleteToken(tokenVal);
    }

    @Override
    public AccountStatus getAccountStatus(String userName) {
        return authDao.getAccountStatus(userName);
    }

    @Override
    public List<AccountInfo> getAccountsInfo() throws AuthException {
        currentUserService.checkUserIsAdminOrRoot();
        List<AccountInfo> allAccounts = authDao.getAccountInfos();
        List<AccountInfo> visibleAccounts = allAccounts
                .stream()
                .filter(accountInfo -> accountInfo.isAtLeast(currentUserService.getStatus()))
                .collect(Collectors.toList());
        return visibleAccounts;
    }

    @Override
    public void changeAccountStatus(String userName, AccountStatus newStatus) throws AuthException {
        // tell don't ask
        currentUserService.checkUserIsAdminOrRoot();
        authDao.changeAccountStatus(userName, newStatus);
    }

    @Override
    public void deleteAccount(String userName) throws AuthException {
        currentUserService.checkUserIsAdminOrRoot();
        authDao.deleteAccount(userName);
    }
}



