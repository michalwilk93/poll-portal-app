package com.portal.domain.impl.ahp.inputTree;

import com.portal.domain.api.types.ComparisonMatrix;

import java.util.List;

public class InputBranch extends InputNode {
    private ComparisonMatrix criteriaPreferences;
    private List<InputNode> subcriteria;

    public ComparisonMatrix getCriteriaPreferences() {
        return criteriaPreferences;
    }

    public void setCriteriaPreferences(ComparisonMatrix criteriaPreferences) {
        this.criteriaPreferences = criteriaPreferences;
    }

    public List<InputNode> getSubcriteria() {
        return subcriteria;
    }

    public void setSubcriteria(List<InputNode> subcriteria) {
        this.subcriteria = subcriteria;
    }
}
