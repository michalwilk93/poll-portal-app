package com.portal.domain.impl.ahp.outputTree;

import java.util.List;

public class OutputBranch extends OutputNode {
    private List<OutputNode> subcriteria;
    private double[] vector;

    public double[] getVector() {
        return vector;
    }

    public void setVector(double[] vector) {
        this.vector = vector;
    }

    public List<OutputNode> getSubcriteria() {
        return subcriteria;
    }

    public void setSubcriteria(List<OutputNode> subcriteria) {
        this.subcriteria = subcriteria;
    }
}
