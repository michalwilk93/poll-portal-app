package com.portal.domain.impl.ahp.outputTree;

import com.portal.domain.impl.ahp.NamedNode;

public class OutputNode extends NamedNode {
    private double[] vector;

    public double[] getVector() {
        return vector;
    }

    public void setVector(double[] vector) {
        this.vector = vector;
    }
}
