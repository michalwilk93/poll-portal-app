package com.portal.domain.impl.ahp.inputTree;

import com.portal.domain.api.types.ComparisonMatrix;

public class InputLeaf extends InputNode {
    private ComparisonMatrix alternativePreferences;

    public ComparisonMatrix getAlternativePreferences() {
        return alternativePreferences;
    }

    public void setAlternativePreferences(ComparisonMatrix alternativePreferences) {
        this.alternativePreferences = alternativePreferences;
    }
}
