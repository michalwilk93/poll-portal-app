package com.portal.domain.impl.ahp.outputTree;


public class OutputLeaf extends OutputNode {
    private double globalWeight;

    public double getGlobalWeight() {
        return globalWeight;
    }

    public void setGlobalWeight(double globalWeight) {
        this.globalWeight = globalWeight;
    }
}
