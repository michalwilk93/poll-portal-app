package com.portal.domain.impl.ahp;

import java.io.Serializable;

public class NamedNode implements Serializable {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
