package com.portal.domain.impl.inconsistency;

import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Threshold;

/*
import org.jblas.ComplexDouble;
import org.jblas.DoubleMatrix;
import org.jblas.Eigen;
*/
public class Saaty extends InconsistencyMethod {
    @Override
    public boolean isConsistent(ComparisonMatrix matrix, Threshold threshold) {
        if (matrix.getSize() < 3) {
            return true;
        }
        /*

         ComplexDouble[] doubleMatrix = Eigen.eigenvalues(new DoubleMatrix(matrix.asDoubleMatrix())).toArray();
        int maxIndex = 0;
        for (int i = 0; i < doubleMatrix.length; i++) {
            double newnumber = doubleMatrix[i].abs();
            if ((newnumber > doubleMatrix[maxIndex].abs())) {
                maxIndex = i;
            }
        }
        double maxEigenvalue = doubleMatrix[maxIndex].abs();
        int n = matrix.getSize();
        double CIN = (maxEigenvalue - (double) n) / (double) (n - 1);
        double RIN = rinValues[n];
        double CRN = CIN / RIN;
        return (CRN <= threshold.getValue());




        * */
        return true;
    }

    private static final double[] rinValues = {0, 0, 0, 0.52, 0.89, 1.11, 1.25, 1.35, 1.4, 1.45, 1.49, 1.51, 1.54, 1.56, 1.57, 1.58};
}
