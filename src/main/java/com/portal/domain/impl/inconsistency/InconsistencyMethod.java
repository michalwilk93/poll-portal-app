package com.portal.domain.impl.inconsistency;

import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Threshold;

public abstract class InconsistencyMethod {
    public abstract boolean isConsistent(ComparisonMatrix matrix,Threshold t);
}
