package com.portal.domain.impl.inconsistency;

import com.portal.domain.api.types.Inconsistency;

public class InconsistencyMethodFactory {
    public InconsistencyMethod getInconsistencyMethod(Inconsistency method) {
        if (method == null) {
            return null;
        }else if (method == Inconsistency.SAATY) {
            return new Saaty();
        }//todo add more inconsistency methods
        return null;
    }
}
