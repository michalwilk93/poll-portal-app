package com.portal.domain.impl;

import com.portal.domain.api.exceptions.*;
import com.portal.domain.api.types.Library;
import com.portal.domain.api.*;
import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.types.Id;
import com.portal.domain.spi.JudgementMethodDao;
import com.portal.domain.spi.PollDao;
import com.portal.domain.spi.StatisticsDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

public class PollMgmtImpl implements PollMgmt {
    private final CurrentUserService currentUserService;
    private final MathEngine engine;
    private final PollDao pollDao;
    private final StatisticsDao statisticsDao;
    private final JudgementMethodDao judgementMethodDao;
    private final Instantiator<JudgementMethod> instantiator;
    private final StatisticsTreeTraverser traveser;

    @Autowired
    public PollMgmtImpl(MathEngine engine,
                        PollDao pollDao,
                        CurrentUserService currentUserService,
                        StatisticsDao statisticsDao,
                        JudgementMethodDao judgementMethodDao,
                        Instantiator<JudgementMethod> instantiator) {
        this.engine = engine;
        this.pollDao = pollDao;
        this.currentUserService = currentUserService;
        this.statisticsDao = statisticsDao;
        this.judgementMethodDao = judgementMethodDao;
        this.instantiator = instantiator;
        this.traveser = new StatisticsTreeTraverser();
    }

    @Override
    public PollResult createFeedBack(FilledPoll filled, String ip) throws PollException {
        JudgementMethod jm = instantiateJudgement(filled);
        PollResult result = engine.computePollResult(filled, jm);
        updateStatistics(result);
        statisticsDao.addRespondentIp(filled.getId(), ip);
        return result;
    }

    @Override
    public boolean checkInconsistency(InconsistencyCheckRequest dataToCheck) {
        return engine.checkInconsistency(dataToCheck);
    }

    @Override
    public void createNewPoll(PollCreationRequest request) throws PollCreationException {
        Id id = new Id(UUID.randomUUID().toString());
        PollDefinition pDef = new PollDefinition(
                id,
                request.getAuthor(),
                request.getProperties(),
                request.getAlternatives(),
                request.getCriteriaTree(),
                request.getAlternativesDescriptions());

        PollRespondents pollRespondents = new PollRespondents(new LinkedList<>(), id);
        pollDao.saveNewPoll(pDef);
        statisticsDao.saveRespondents(pollRespondents);
    }


    @Override
    public PollDefinition takePoll(Id pollId, String ip) throws IpUsedException {
        if (statisticsDao.getPollRespondents(pollId).getRespondentsIps().contains(ip)) {
            throw new IpUsedException("Your Ip address has already taken this poll.", ErrorCodes.IP_ADDRESS_USED);
        } else {
            return pollDao.getPoll(pollId);
        }
    }

    @Override
    public List<PollDefinition> getPublicPolls() {
        return pollDao.getPublicPolls();
    }

    @Override
    public List<PollDefinition> getUserPolls(String userName) {
        return pollDao.getUserPolls(userName);
    }

    @Override
    public PollResult getGeneralStatistics(Id id) {
        return statisticsDao.getPollStatistics(id);
    }

    @Override
    public void updatePoll(Id id, PollEditableProperties newPollEditableProperties) throws PollCreationException {
        pollDao.updateEditableProperties(id, newPollEditableProperties);
    }

    @Override
    public List<PollDefinition> getCurrentUserPolls() {
        return pollDao.getUserPolls(currentUserService.getUserName());
    }

    @Override
    public void deletePoll(Id id) throws AuthException {
        if (!isPollCreatedByCurrentUser(id)) {
            currentUserService.checkUserIsAdminOrRoot();
        }
        pollDao.deletePoll(id);
    }

    private boolean isPollCreatedByCurrentUser(Id pollId) {
        PollDefinition pollDefinition = pollDao.getPoll(pollId);
        return currentUserService.getUserName().equals(pollDefinition.getAuthor());
    }

    private JudgementMethod instantiateJudgement(FilledPoll filled) throws PollException {
        JudgementMethodDefinition judgementMethodDef = judgementMethodDao.getJudgementMethod(filled.getJudgementMethodId());
        List<Library> userLibraries = judgementMethodDao.getLibsWithIds(judgementMethodDef.getData().getLibsIds());
        return instantiator.instantiate(JudgementMethod.class,judgementMethodDef.getCompiledFunctionalClass(), userLibraries);
    }

    private void updateStatistics(PollResult newResult) {
        PollResult previousStatistics = statisticsDao.getPollStatistics(newResult.getPollId());
        if (previousStatistics == null) {
            newResult.setRespondentCount(1);
            statisticsDao.saveStatistics(newResult);
        } else {
            int oldRespondentCount = previousStatistics.getRespondentCount();
            traveser.updateStatisticsTree(previousStatistics.getRoot(), newResult.getRoot(), oldRespondentCount);
            newResult.setRespondentCount(oldRespondentCount + 1);
            statisticsDao.saveStatistics(previousStatistics);
        }
    }
}
