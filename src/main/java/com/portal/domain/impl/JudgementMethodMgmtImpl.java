package com.portal.domain.impl;

import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.JudgementMethodMgmt;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.CompilationException;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Library;
import com.portal.domain.spi.JudgementMethodDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

public class JudgementMethodMgmtImpl implements JudgementMethodMgmt {
    private final CurrentUserService currentUserService;
    private final JudgementMethodDao judgementMethodDao;
    private final FunctionalClassCompiler functionalClassCompiler;
    @Autowired
    public JudgementMethodMgmtImpl(
            CurrentUserService currentUserService,
            JudgementMethodDao judgementMethodDao,
            FunctionalClassCompiler functionalClassCompiler) {
        this.currentUserService = currentUserService;
        this.judgementMethodDao = judgementMethodDao;
        this.functionalClassCompiler = functionalClassCompiler;
    }

    @Override
    public void addLibrary(byte[] bytes, String fileName) throws IOException, AuthException {
        currentUserService.checkUserIsRoot();
        Id id = new Id(UUID.randomUUID().toString());
        Library library = new Library(id, fileName, bytes);
        judgementMethodDao.saveLibrary(library);
    }

    @Override
    public List<Library> getLibraries() throws AuthException {
        currentUserService.checkUserIsRoot();
        return judgementMethodDao.getLibraries();
    }

    @Override
    public List<JudgementMethodDefinition> getCustomJudgementMethods() throws AuthException {
        return judgementMethodDao.getCustomJudgementMethods();
    }

    @Override
    public void editJudgementMethod(Id id, FileContent fileContent) throws AuthException {
        currentUserService.checkUserIsRoot();
        judgementMethodDao.editJudgementMethod(id, fileContent);
    }

    @Override
    public void createJudgementMethod(FileContent fileContent) throws AuthException, CompilationException {
        currentUserService.checkUserIsRoot();
        List<Library> userLibraries = judgementMethodDao.getLibsWithIds(fileContent.getLibsIds());
        byte[] compiledClass  =
                functionalClassCompiler.compileImplementationOf(JudgementMethod.class.getName(), fileContent, userLibraries);
        saveMethod(fileContent,compiledClass);
    }


    private String writeFileOnDisc(byte[] bytes, String fileName) throws IOException {
        String directory = ".";
        String filepath = Paths.get(directory, fileName).toString();
        BufferedOutputStream stream =
                new BufferedOutputStream(new FileOutputStream(new File(filepath)));
        stream.write(bytes);
        stream.close();
        return filepath;
    }


    private void saveMethod(FileContent fileContent,byte[] classBytes) {
        Id id = new Id(UUID.randomUUID().toString());
        JudgementMethodDefinition judgementMethodDefinition = new JudgementMethodDefinition(id, fileContent,classBytes);
        judgementMethodDao.saveNewJudgementMethod(judgementMethodDefinition);
    }
}
