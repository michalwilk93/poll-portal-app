package com.portal;

import com.portal.domain.api.exceptions.PollException;
import com.portal.rest.dataTransferObjects.RestRequestResult;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalDefaultExceptionHandler {
    @ExceptionHandler(value = {PollException.class})
    public @ResponseBody RestRequestResult defaultErrorHandler(PollException e) throws Exception {
        RestRequestResult result = new RestRequestResult();
        result.setSuccessful(false);
        result.setMessageCode(e.getErrorCode().getCodeVal());
        result.setMessage(e.getErrorMessage());
        return result;
    }
}
