package com.portal.db.dataObjects;


import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
@Entity
public class PollRespondentsDo {
    @Id
    private String pollId;

    @ElementCollection
    private List<String> respondentsIps;

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public void setRespondentsIps(List<String> respondentsIps) {
        this.respondentsIps = respondentsIps;
    }

    public String getPollId() {
        return pollId;
    }

    public List<String> getRespondentsIps() {
        return respondentsIps;
    }
}
