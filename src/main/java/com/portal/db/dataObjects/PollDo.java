package com.portal.db.dataObjects;

import com.portal.domain.api.types.*;
import com.portal.domain.api.types.Node;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.List;

@Entity
public class PollDo {
    @Id
    private String id;
    private String inconsistencyMethod;
    private String author;
    private String access;
    private String name;
    private String description;
    @ElementCollection
    private List<String> alternatives;
    private double minGrade;
    private double maxGrade;
    private double scaleInterval;
    private double inconsistencyThreshold;
    private String judgementMethodId;
    @Lob
    Descriptions alternativesDescriptions;
    @Lob
    Node criteriaTree;

    public String getJudgementMethodId() {
        return judgementMethodId;
    }

    public void setJudgementMethodId(String judgementMethodId) {
        this.judgementMethodId = judgementMethodId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Descriptions getAlternativesDescriptions() {
        return alternativesDescriptions;
    }

    public void setAlternativesDescriptions(Descriptions alternativesDescriptions) {
        this.alternativesDescriptions = alternativesDescriptions;
    }

    public Node getCriteriaTree() {
        return criteriaTree;
    }

    public void setCriteriaTree(Node criteriaTree) {
        this.criteriaTree = criteriaTree;
    }

    public double getScaleInterval() {
        return scaleInterval;
    }

    public void setScaleInterval(double scaleInterval) {
        this.scaleInterval = scaleInterval;
    }

    public String getInconsistencyMethod() {
        return inconsistencyMethod;
    }

    public void setInconsistencyMethod(String inconsistencyMethod) {
        this.inconsistencyMethod = inconsistencyMethod;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<String> alternatives) {
        this.alternatives = alternatives;
    }

    public double getMinGrade() {
        return minGrade;
    }

    public void setMinGrade(double minGrade) {
        this.minGrade = minGrade;
    }

    public double getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(double maxGrade) {
        this.maxGrade = maxGrade;
    }

    public double getInconsistencyThreshold() {
        return inconsistencyThreshold;
    }

    public void setInconsistencyThreshold(double inconsistencyThreshold) {
        this.inconsistencyThreshold = inconsistencyThreshold;
    }
}
