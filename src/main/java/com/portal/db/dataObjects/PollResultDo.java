package com.portal.db.dataObjects;

import com.portal.domain.impl.ahp.outputTree.OutputBranch;

import javax.persistence.Entity;
import javax.persistence.Lob;

@Entity
public class PollResultDo {
    @javax.persistence.Id
    private String pollId;
    @Lob
    private OutputBranch root;
    private int respondentCount;

    public OutputBranch getRoot() {
        return root;
    }

    public void setRoot(OutputBranch root) {
        this.root = root;
    }

    public String getPollId() {
        return pollId;
    }

    public void setPollId(String pollId) {
        this.pollId = pollId;
    }

    public int getRespondentCount() {
        return respondentCount;
    }

    public void setRespondentCount(int respondentCount) {
        this.respondentCount = respondentCount;
    }
}
