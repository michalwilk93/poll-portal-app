package com.portal.db.dataObjects;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import java.util.List;

@Entity
public class JudgementMethodDo {
    @Id
    private String id;
    private String name;
    @Lob
    private String imports;
    @Lob
    private String code;
    @ElementCollection
    private List<String> libs;

    @Lob
    private byte[] compiledClass;

    public byte[] getCompiledClass() {
        return compiledClass;
    }

    public void setCompiledClass(byte[] compiledClass) {
        this.compiledClass = compiledClass;
    }

    public List<String> getLibs() {
        return libs;
    }

    public void setLibs(List<String> libs) {
        this.libs = libs;
    }

    public String getImports() {
        return imports;
    }

    public void setImports(String imports) {
        this.imports = imports;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
