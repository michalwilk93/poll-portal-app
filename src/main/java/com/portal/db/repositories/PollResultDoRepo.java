package com.portal.db.repositories;

import com.portal.db.dataObjects.PollResultDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollResultDoRepo extends JpaRepository<PollResultDo, String> {
}