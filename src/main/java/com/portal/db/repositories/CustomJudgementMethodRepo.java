package com.portal.db.repositories;

import com.portal.db.dataObjects.JudgementMethodDo;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.FileContent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;


public interface CustomJudgementMethodRepo extends JpaRepository<JudgementMethodDo, String> {


    @Transactional
    @Modifying
    default public void updateMethod(Id id, FileContent fileContent){
        JudgementMethodDo judgementMethodDo = this.findOne(id.toString());
        judgementMethodDo.setName(fileContent.getName());
        judgementMethodDo.setCode(fileContent.getCode());
        judgementMethodDo.setImports(fileContent.getImports());
        this.save(judgementMethodDo);
    }
}
