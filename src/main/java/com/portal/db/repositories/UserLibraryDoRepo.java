package com.portal.db.repositories;


import com.portal.db.dataObjects.UserLibraryDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserLibraryDoRepo extends JpaRepository<UserLibraryDo, String> {
}