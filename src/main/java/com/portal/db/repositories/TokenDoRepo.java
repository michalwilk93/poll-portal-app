package com.portal.db.repositories;

import com.portal.db.dataObjects.TokenDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TokenDoRepo extends JpaRepository<TokenDo, String> {
}