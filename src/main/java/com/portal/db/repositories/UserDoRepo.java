package com.portal.db.repositories;


import com.portal.db.dataObjects.UserDo;
import com.portal.domain.api.exceptions.AccountCreationException;
import com.portal.domain.api.exceptions.ErrorCodes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface UserDoRepo extends JpaRepository<UserDo, String> {
    @Transactional
    default public void createNewAccount(UserDo userDo) throws AccountCreationException {
        UserDo previous = this.findOne(userDo.getName());
        if (previous == null) {
            throw new AccountCreationException("This user name already exists", ErrorCodes.USER_ALREADY_EXISTS);
        } else {
            this.save(userDo);
        }
    }
}