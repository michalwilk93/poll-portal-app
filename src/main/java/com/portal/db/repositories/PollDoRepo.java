package com.portal.db.repositories;


import com.portal.db.dataObjects.PollDo;
import com.portal.domain.api.PollEditableProperties;
import com.portal.domain.api.types.Id;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface PollDoRepo extends JpaRepository<PollDo, String> {
    List<PollDo> findByAuthor(String author);
    List<PollDo> findByAccessIgnoreCase(String access);

    @Transactional
    @Modifying
    default public void updateEditableProperties(Id pollId, PollEditableProperties editableProperties) {
        PollDo pdo = this.findOne(pollId.toString());
        pdo.setName(editableProperties.getName());
        pdo.setDescription(editableProperties.getDescription());
        pdo.setJudgementMethodId(editableProperties.getJudgementMethodId().toString());
        pdo.setAccess(editableProperties.getAccess().toString());
        pdo.setInconsistencyMethod(editableProperties.getInconsistencyMethod().toString());
        pdo.setMaxGrade(editableProperties.getScale().getMax());
        pdo.setMinGrade(editableProperties.getScale().getMin());
        pdo.setScaleInterval(editableProperties.getScale().getInterval());
        pdo.setInconsistencyThreshold(editableProperties.getInconsistencyThreshold().getValue());
        this.save(pdo);
    }
}