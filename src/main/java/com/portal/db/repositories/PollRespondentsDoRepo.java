package com.portal.db.repositories;

import com.portal.db.dataObjects.PollRespondentsDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PollRespondentsDoRepo extends JpaRepository<PollRespondentsDo, String> {
}
