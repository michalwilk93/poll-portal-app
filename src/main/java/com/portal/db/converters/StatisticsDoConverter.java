package com.portal.db.converters;

import com.portal.db.dataObjects.PollRespondentsDo;
import com.portal.db.dataObjects.PollResultDo;
import com.portal.domain.api.PollRespondents;
import com.portal.domain.api.PollResult;
import com.portal.domain.api.types.Id;

import java.util.List;

public class StatisticsDoConverter {

    public PollResultDo toDataObject(PollResult result) {
        PollResultDo res = new PollResultDo();
        res.setPollId(result.getPollId().toString());
        res.setRespondentCount(result.getRespondentCount());
        res.setRoot(result.getRoot());
        return res;
    }

    public PollResult fromDataObject(PollResultDo resultDo) {
        if (resultDo == null) return null;
        Id pollId = new Id(resultDo.getPollId());
        PollResult res = new PollResult(resultDo.getRoot(), pollId);
        res.setRespondentCount(resultDo.getRespondentCount());
        return res;
    }

    public PollRespondents fromDataObject(PollRespondentsDo pollRespondentsDo) {
        Id respondentsListId = new Id(pollRespondentsDo.getPollId());
        List<String> respondentsIp = pollRespondentsDo.getRespondentsIps();
        return new PollRespondents(respondentsIp, respondentsListId);
    }

    public PollRespondentsDo toDataObject(PollRespondents pollRespondents) {
        PollRespondentsDo pollRespondentsDo = new PollRespondentsDo();
        pollRespondentsDo.setPollId(pollRespondents.getPollId().toString());
        pollRespondentsDo.setRespondentsIps(pollRespondents.getRespondentsIps());
        return pollRespondentsDo;
    }

}
