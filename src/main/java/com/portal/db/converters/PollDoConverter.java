package com.portal.db.converters;

import com.portal.db.dataObjects.PollDo;
import com.portal.domain.api.PollDefinition;
import com.portal.domain.api.PollEditableProperties;
import com.portal.domain.api.types.*;

import java.util.ArrayList;
import java.util.List;

public class PollDoConverter {
    public List<PollDefinition> fromDataObjectList(List<PollDo> pollsDo) {
        List<PollDefinition> result = new ArrayList<>();
        for (PollDo pollDo : pollsDo) {
            result.add(fromDataObject(pollDo));
        }
        return result;
    }

    public PollDo toDataObject(PollDefinition p) {
        PollDo pdo = new PollDo();
        pdo.setAuthor(p.getAuthor());
        pdo.setId(p.getId().toString());
        pdo.setCriteriaTree(p.getCriteriaTree());
        pdo.setAlternatives(p.getAlternatives().getAlternatives());
        pdo.setAlternativesDescriptions(p.getAlternativesDescriptions());

        PollEditableProperties editableProperties = p.getEditableProperties();
        pdo.setName(editableProperties.getName());
        pdo.setDescription(editableProperties.getDescription());
        pdo.setJudgementMethodId(editableProperties.getJudgementMethodId().toString());
        pdo.setAccess(editableProperties.getAccess().toString());
        pdo.setInconsistencyMethod(editableProperties.getInconsistencyMethod().toString());
        pdo.setMaxGrade(editableProperties.getScale().getMax());
        pdo.setMinGrade(editableProperties.getScale().getMin());
        pdo.setScaleInterval(editableProperties.getScale().getInterval());
        pdo.setInconsistencyThreshold(editableProperties.getInconsistencyThreshold().getValue());
        return pdo;
    }

    public PollDefinition fromDataObject(PollDo pdo) {
        PollEditableProperties editableProperties = convertEditableProperties(pdo);
        Alternatives alternatives = new Alternatives(pdo.getAlternatives());
        Id pollId = new Id(pdo.getId());

        return new PollDefinition(
                pollId,
                pdo.getAuthor(),
                editableProperties,
                alternatives,
                pdo.getCriteriaTree(),
                pdo.getAlternativesDescriptions());
    }

    private PollEditableProperties convertEditableProperties(PollDo pdo) {
        GradeScale gradeScale = new GradeScale(pdo.getMinGrade(), pdo.getMaxGrade(), pdo.getScaleInterval());
        Threshold threshold = new Threshold(pdo.getInconsistencyThreshold());
        Id judgementMethodId = new Id(pdo.getJudgementMethodId());

        return new PollEditableProperties(
                pdo.getName(),
                pdo.getDescription(),
                Inconsistency.valueOf(pdo.getInconsistencyMethod()),
                gradeScale,
                threshold,
                judgementMethodId,
                Access.valueOf(pdo.getAccess()));
    }
}
