package com.portal.db.converters;


import com.portal.db.dataObjects.UserLibraryDo;
import com.portal.domain.api.types.Library;
import com.portal.domain.api.types.Id;

import java.util.ArrayList;
import java.util.List;

public class LibraryDoConverter {
    public UserLibraryDo toDataObject(Library library) {
        UserLibraryDo userLibraryDo = new UserLibraryDo();
        userLibraryDo.setFileName(library.getFileName());
        userLibraryDo.setId(library.getId().toString());
        userLibraryDo.setBytes(library.getBytes());
        return userLibraryDo;
    }

    public Library fromDataObject(UserLibraryDo userLibraryDo) {
        Id libId = new Id(userLibraryDo.getId());
        return new Library(libId, userLibraryDo.getFileName(), userLibraryDo.getBytes());
    }

    public List<Library> fromDataObject(List<UserLibraryDo> userLibraryDos) {
        List<Library> result = new ArrayList<>();
        for (UserLibraryDo userLibraryDo : userLibraryDos) {
            result.add(fromDataObject(userLibraryDo));
        }
        return result;
    }
}
