package com.portal.db.converters;

import com.portal.domain.api.types.AccountInfo;
import com.portal.domain.api.types.Token;
import com.portal.domain.api.types.UserCredentials;
import com.portal.domain.api.types.AccountStatus;
import com.portal.db.dataObjects.TokenDo;
import com.portal.db.dataObjects.UserDo;

import java.util.LinkedList;
import java.util.List;

public class AuthDoConverter {

    public TokenDo toDataObject(Token t) {
        TokenDo tdo = new TokenDo();
        tdo.setCreationTime(t.getCreationTime());
        tdo.setIp(t.getIp());
        tdo.setUser(t.getUserName());
        tdo.setTokenValue(t.getTokenValue());
        return tdo;
    }

    public UserDo toDataObject(UserCredentials credentials) {
        UserDo ucdo = new UserDo();
        ucdo.setName(credentials.getName());
        ucdo.setPassword(credentials.getPassword());
        ucdo.setStatus(credentials.getStatus().toString());
        return ucdo;
    }

    public AccountInfo fromDataObject(UserDo userDo) {
        AccountStatus accountStatus = AccountStatus.valueOf(userDo.getStatus().toUpperCase());
        return new AccountInfo(userDo.getName(), accountStatus);
    }

    public List<AccountInfo> fromDataObject(List<UserDo> userDos) {
        List<AccountInfo> accountInfos = new LinkedList<>();
        for (UserDo userDo : userDos) {
            accountInfos.add(fromDataObject(userDo));
        }
        return accountInfos;
    }
}
