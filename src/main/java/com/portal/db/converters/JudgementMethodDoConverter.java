package com.portal.db.converters;


import com.portal.db.dataObjects.JudgementMethodDo;
import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Id;

import java.util.LinkedList;
import java.util.List;

public class JudgementMethodDoConverter {

    public JudgementMethodDefinition fromDataObject(JudgementMethodDo methodDo) {
        Id methodId = new Id(methodDo.getId());
        List<Id> libsIds = new LinkedList<>();
        for (String libId : methodDo.getLibs()) {
            libsIds.add(new Id(libId));
        }
        FileContent fileContent = new FileContent(
                methodDo.getCode(),
                methodDo.getName(),
                methodDo.getImports(),
                libsIds);
        return new JudgementMethodDefinition(methodId, fileContent,methodDo.getCompiledClass());
    }

    public JudgementMethodDo toDataObject(JudgementMethodDefinition judgementMethodDefinition) {
        JudgementMethodDo judgementMethodDo = new JudgementMethodDo();
        judgementMethodDo.setCode(judgementMethodDefinition.getData().getCode());
        judgementMethodDo.setName(judgementMethodDefinition.getData().getName());
        judgementMethodDo.setImports(judgementMethodDefinition.getData().getImports());
        judgementMethodDo.setId(judgementMethodDefinition.getMethodId().toString());

        List<String> libsIdStrings = new LinkedList<>();
        List<Id> libsIds = judgementMethodDefinition.getData().getLibsIds();
        for (Id libId : libsIds) {
            libsIdStrings.add(libId.toString());
        }
        judgementMethodDo.setLibs(libsIdStrings);
        judgementMethodDo.setCompiledClass(judgementMethodDefinition.getCompiledFunctionalClass());
        return judgementMethodDo;
    }

    public List<JudgementMethodDefinition> fromDataObject(List<JudgementMethodDo> judgementMethodDos) {
        List<JudgementMethodDefinition> judgementMethodDefinitions = new LinkedList<>();
        for (JudgementMethodDo method : judgementMethodDos) {
            judgementMethodDefinitions.add(fromDataObject(method));
        }
        return judgementMethodDefinitions;
    }
}
