package com.portal.db.daos;

import com.portal.db.converters.StatisticsDoConverter;
import com.portal.db.dataObjects.PollRespondentsDo;
import com.portal.db.dataObjects.PollResultDo;
import com.portal.db.repositories.PollRespondentsDoRepo;
import com.portal.db.repositories.PollResultDoRepo;
import com.portal.domain.api.PollRespondents;
import com.portal.domain.api.PollResult;
import com.portal.domain.api.types.Id;
import com.portal.domain.spi.StatisticsDao;
import org.springframework.beans.factory.annotation.Autowired;

public class StatisticsDaoImpl implements StatisticsDao {
    private final PollResultDoRepo pollResultDoRepo;
    private final StatisticsDoConverter statisticsDoConverter;
    private final PollRespondentsDoRepo pollRespondentsDoRepo;

    @Autowired
    public StatisticsDaoImpl(PollResultDoRepo pollResultDoRepo,
                             PollRespondentsDoRepo pollRespondentsDoRepo) {
        this.pollResultDoRepo = pollResultDoRepo;
        this.statisticsDoConverter = new StatisticsDoConverter();
        this.pollRespondentsDoRepo = pollRespondentsDoRepo;
    }

    @Override
    public void saveStatistics(PollResult pr) {
        pollResultDoRepo.save(statisticsDoConverter.toDataObject(pr));
    }


    @Override
    public PollResult getPollStatistics(Id pollId) {
        String id = pollId.toString();
        PollResultDo prdo = pollResultDoRepo.findOne(id);
        return statisticsDoConverter.fromDataObject(prdo);
    }

    @Override
    public void saveRespondents(PollRespondents pollRespondents) {
        pollRespondentsDoRepo.save(statisticsDoConverter.toDataObject(pollRespondents));
    }


    @Override
    public PollRespondents getPollRespondents(Id pollId) {
        PollRespondentsDo pollRespondentsDo = pollRespondentsDoRepo.findOne(pollId.toString());
        return statisticsDoConverter.fromDataObject(pollRespondentsDo);
    }

    @Override
    public void addRespondentIp(Id pollId, String ip) {
        PollRespondentsDo pollRespondentsDo = pollRespondentsDoRepo.findOne(pollId.toString());
        pollRespondentsDo.getRespondentsIps().add(ip);
        pollRespondentsDoRepo.save(pollRespondentsDo);
    }
}
