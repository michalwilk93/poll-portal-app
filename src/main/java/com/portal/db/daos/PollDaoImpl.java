package com.portal.db.daos;

import com.portal.db.converters.PollDoConverter;
import com.portal.db.dataObjects.*;
import com.portal.db.repositories.*;
import com.portal.domain.api.PollEditableProperties;
import com.portal.domain.api.exceptions.PollCreationException;
import com.portal.domain.api.PollDefinition;
import com.portal.domain.api.types.Id;
import com.portal.domain.spi.PollDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class PollDaoImpl implements PollDao {
    private final PollDoRepo pollDoRepo;
    private final PollDoConverter pollDoConverter;

    @Autowired
    public PollDaoImpl(PollDoRepo pollDoRepo) {
        this.pollDoRepo = pollDoRepo;
        this.pollDoConverter = new PollDoConverter();
    }

    @Override
    public void saveNewPoll(PollDefinition p) throws PollCreationException {
        pollDoRepo.save(pollDoConverter.toDataObject(p));
    }

    @Override
    public void updateEditableProperties(Id pollId, PollEditableProperties pollEditableProperties) {
        pollDoRepo.updateEditableProperties(pollId, pollEditableProperties);
    }

    @Override
    public List<PollDefinition> getUserPolls(String userName) {
        List<PollDo> pollsDo = pollDoRepo.findByAuthor(userName);
        return pollDoConverter.fromDataObjectList(pollsDo);
    }

    @Override
    public PollDefinition getPoll(Id pollId) {
        String id = pollId.toString();
        return pollDoConverter.fromDataObject(pollDoRepo.findOne(id));
    }

    @Override
    public List<PollDefinition> getPublicPolls() {
        List<PollDo> pollDos = pollDoRepo.findByAccessIgnoreCase("public");
        return pollDoConverter.fromDataObjectList(pollDos);
    }

    @Override
    public void deletePoll(Id id) {
        pollDoRepo.delete(id.toString());
    }
}
