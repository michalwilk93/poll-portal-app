package com.portal.db.daos;

import com.portal.db.repositories.TokenDoRepo;
import com.portal.db.repositories.UserDoRepo;
import com.portal.db.converters.AuthDoConverter;
import com.portal.db.dataObjects.TokenDo;
import com.portal.db.dataObjects.UserDo;
import com.portal.domain.api.exceptions.AccountCreationException;
import com.portal.domain.api.types.*;
import com.portal.domain.spi.AuthDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AuthDaoImpl implements AuthDao {
    private final UserDoRepo userDoRepo;
    private final TokenDoRepo tokenDoRepo;
    private final AuthDoConverter authDoConverter;

    @Autowired
    public AuthDaoImpl(UserDoRepo userDoRepo, TokenDoRepo tokenDoRepo) {
        this.userDoRepo = userDoRepo;
        this.tokenDoRepo = tokenDoRepo;
        this.authDoConverter = new AuthDoConverter();
    }

    @Override
    public void createAccount(UserCredentials uc) throws AccountCreationException {
        userDoRepo.createNewAccount(authDoConverter.toDataObject(uc));
    }

    @Override
    public UserAuthentication identify(String tokenVal, String remoteAddr) {
        TokenDo tokenDo = tokenDoRepo.findOne(tokenVal);
        if (tokenDo != null && tokenDo.getIp().equals(remoteAddr)) {
            if (tokenHasLessThan24Hours(tokenDo.getCreationTime())) {
                AccountStatus status = getAccountStatus(tokenDo.getUser());
                return new UserAuthentication(status,tokenDo.getUser());
            }
        }
        return null;
    }

    @Override
    public boolean credentialsOk(String name, String password) {
        UserDo userDo = userDoRepo.findOne(name);
        return userDo != null && userDo.getPassword().equals(password) ;
    }

    @Override
    public void saveToken(Token t) {
        tokenDoRepo.save(authDoConverter.toDataObject(t));
    }

    @Override
    public void deleteToken(String tokenVal) {
        tokenDoRepo.delete(tokenVal);
    }

    @Override
    public AccountStatus getAccountStatus(String userName) {
        UserDo userDo = userDoRepo.findOne(userName);
        return AccountStatus.valueOf(userDo.getStatus().toUpperCase());
    }

    @Override
    public List<AccountInfo> getAccountInfos() {
        List<UserDo> userDos = userDoRepo.findAll();
        return authDoConverter.fromDataObject(userDos);
    }

    @Override
    public void changeAccountStatus(String userName, AccountStatus newStatus) {
        UserDo userDo = userDoRepo.findOne(userName);
        userDo.setStatus(newStatus.toString());
    }

    @Override
    public void deleteAccount(String userName) {
        userDoRepo.delete(userName);
    }

    private boolean tokenHasLessThan24Hours(Timestamp creationTime) {
        Date dt = getDate24HoursEarlier();
        return dt.before(creationTime);
    }

    private Date getDate24HoursEarlier() {
        Date dt = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dt);
        c.add(Calendar.HOUR, -24);
        return c.getTime();
    }
}
