package com.portal.db.daos;


import com.portal.db.converters.JudgementMethodDoConverter;
import com.portal.db.converters.LibraryDoConverter;
import com.portal.db.dataObjects.JudgementMethodDo;
import com.portal.db.dataObjects.UserLibraryDo;
import com.portal.db.repositories.CustomJudgementMethodRepo;
import com.portal.db.repositories.UserLibraryDoRepo;
import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.exceptions.ErrorCodes;
import com.portal.domain.api.exceptions.PollException;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Library;
import com.portal.domain.spi.JudgementMethodDao;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.LinkedList;
import java.util.List;

public class JudgementMethodDaoImpl implements JudgementMethodDao {
    private final JudgementMethodDoConverter judgementMethodDoConverter;
    private final CustomJudgementMethodRepo customJudgementMethodRepo;
    private final LibraryDoConverter libraryDoConverter;
    private final UserLibraryDoRepo userLibraryDoRepo;

    @Autowired
    public JudgementMethodDaoImpl(CustomJudgementMethodRepo customJudgementMethodRepo,
                                  UserLibraryDoRepo userLibraryDoRepo) {
        this.judgementMethodDoConverter = new JudgementMethodDoConverter();
        this.libraryDoConverter = new LibraryDoConverter();
        this.customJudgementMethodRepo = customJudgementMethodRepo;
        this.userLibraryDoRepo = userLibraryDoRepo;
    }

    @Override
    public void saveNewJudgementMethod(JudgementMethodDefinition judgementMethodDefinition) {
        customJudgementMethodRepo.save(judgementMethodDoConverter.toDataObject(judgementMethodDefinition));
    }

    @Override
    public List<JudgementMethodDefinition> getCustomJudgementMethods() {
        return judgementMethodDoConverter.fromDataObject(customJudgementMethodRepo.findAll());
    }

    @Override
    public void editJudgementMethod(Id id, FileContent fileContent) {
        customJudgementMethodRepo.updateMethod(id, fileContent);
    }

    @Override
    public JudgementMethodDefinition getJudgementMethod(Id methodId) throws PollException {
        JudgementMethodDo judgementMethodDo = customJudgementMethodRepo.findOne(methodId.toString());
        if (judgementMethodDo == null) {
            throw new PollException("Chosen judgement method is not available.", ErrorCodes.NO_SUCH_METHOD);
        }
        return judgementMethodDoConverter.fromDataObject(judgementMethodDo);
    }

    @Override
    public void saveLibrary(Library library) {
        userLibraryDoRepo.save(libraryDoConverter.toDataObject(library));
    }

    @Override
    public List<Library> getLibraries() {
        List<UserLibraryDo> userLibraryDos = userLibraryDoRepo.findAll();
        return libraryDoConverter.fromDataObject(userLibraryDos);
    }

    @Override
    public List<Library> getLibsWithIds(List<Id> libsIds) {
        List<String> idList = new LinkedList<>();
        for (Id libsId : libsIds) {
            idList.add(libsId.toString());
        }
        return libraryDoConverter.fromDataObject(userLibraryDoRepo.findAll(idList));
    }
}
