package com.portal;

import com.portal.db.daos.AuthDaoImpl;
import com.portal.db.daos.JudgementMethodDaoImpl;
import com.portal.db.daos.PollDaoImpl;
import com.portal.db.daos.StatisticsDaoImpl;
import com.portal.db.repositories.*;
import com.portal.domain.api.AccountMgmt;
import com.portal.domain.api.JudgementMethodMgmt;
import com.portal.domain.api.MathEngine;
import com.portal.domain.api.PollMgmt;
import com.portal.domain.impl.*;
import com.portal.domain.spi.AuthDao;
import com.portal.domain.spi.JudgementMethodDao;
import com.portal.domain.spi.PollDao;
import com.portal.domain.spi.StatisticsDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.servlet.MultipartConfigElement;

@Configuration
public class BeansConfig extends WebMvcConfigurerAdapter {
    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setMaxFileSize("51MB");
        factory.setMaxRequestSize("51MB");
        return factory.createMultipartConfig();
    }

    @Bean
    @Autowired
    AuthDao authDao(UserDoRepo userDoRepo, TokenDoRepo tokenDoRepo) {
        return new AuthDaoImpl(userDoRepo, tokenDoRepo);
    }

    @Bean
    @Autowired
    PollDao pollDao(PollDoRepo pollDoRepo) {
        return new PollDaoImpl(pollDoRepo);
    }

    @Bean
    @Autowired
    StatisticsDao statisticsDao(PollRespondentsDoRepo pollRespondentsDoRepo,
                                PollResultDoRepo pollResultDoRepo) {
        return new StatisticsDaoImpl(pollResultDoRepo, pollRespondentsDoRepo);
    }

    @Bean
    @Autowired
    JudgementMethodDao judgementMethodDao(CustomJudgementMethodRepo customJudgementMethodRepo,
                                          UserLibraryDoRepo userLibraryDoRepo) {
        return new JudgementMethodDaoImpl(customJudgementMethodRepo, userLibraryDoRepo);
    }

    @Bean
    @Autowired
    PollMgmt pollMgmtImpl(MathEngine me,
                          PollDao pollDao,
                          CurrentUserService currentUserService,
                          StatisticsDao statisticsDao,
                          JudgementMethodDao judgementMethodDao) {
        return new PollMgmtImpl(me, pollDao, currentUserService, statisticsDao, judgementMethodDao, new Instantiator<>());
    }

    @Bean
    @Autowired
    AccountMgmt accountMgmtImpl(AuthDao authDao, CurrentUserService currentUserService) {
        return new AccountMgmtImpl(authDao, currentUserService);
    }

    @Bean
    @Autowired
    @Scope(value = WebApplicationContext.SCOPE_REQUEST,
            proxyMode = ScopedProxyMode.TARGET_CLASS)
    CurrentUserService currentUserService(AuthDao authDao) {
        return new CurrentUserService(authDao);
    }

    @Bean
    @Autowired
    MathEngine mathEngineImpl() {
        return new MathEngineImpl();
    }

    @Bean
    @Autowired
    JudgementMethodMgmt judgementMethodMgmt(CurrentUserService currentUserService,
                                            JudgementMethodDao judgementMethodDao) {
        return new JudgementMethodMgmtImpl(currentUserService, judgementMethodDao, new  FunctionalClassCompiler());
    }
}
