package com.portal.rest;

import com.portal.domain.api.exceptions.ErrorCodes;
import com.portal.domain.impl.CurrentUserService;
import com.portal.domain.api.exceptions.AuthException;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Cookie;

public class AuthInterceptor extends HandlerInterceptorAdapter {
    private CurrentUserService currentUserService;

    public AuthInterceptor(CurrentUserService currentUserService) {
        this.currentUserService = currentUserService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request,
                             HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        String token = null;
        for (Cookie c : cookies) {
            if (c.getName().equals("X-Access-Token")) {
                token = c.getValue();
            }
        }
        if (token == null) {
            throw new AuthException("No token", ErrorCodes.NO_TOKEN);
        }
        currentUserService.identifyCurrentUser(token, request.getRemoteAddr());
        return true;
    }
}





