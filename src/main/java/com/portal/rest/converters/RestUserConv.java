package com.portal.rest.converters;

import com.portal.domain.api.types.AccountInfo;
import com.portal.domain.api.types.UserCredentials;
import com.portal.domain.api.types.AccountStatus;
import com.portal.rest.dataTransferObjects.RestAccountInfo;
import com.portal.rest.dataTransferObjects.RestCredentials;

import java.util.ArrayList;
import java.util.List;

public class RestUserConv {
    public UserCredentials fromDto(RestCredentials rc) {
        return new UserCredentials(rc.getName(), rc.getPassword(), AccountStatus.USER);
    }

    public List<RestAccountInfo> toDto(List<AccountInfo> accountInfos) {
        List<RestAccountInfo> res = new ArrayList<>();
        for (AccountInfo info : accountInfos) {
            RestAccountInfo restAccountInfo = new RestAccountInfo();
            restAccountInfo.setStatus(info.getStatus().toString());
            restAccountInfo.setName(info.getName());
            res.add(restAccountInfo);
        }
        return res;
    }
}
