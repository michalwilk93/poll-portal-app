package com.portal.rest.converters;

import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.types.FileContent;
import com.portal.domain.api.types.Id;
import com.portal.rest.dataTransferObjects.RestJudgementMethod;
import com.portal.rest.dataTransferObjects.RestJudgementMethodData;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class JudgementRestConv {

    public FileContent fromDto(RestJudgementMethodData restJudgementMethodData) {
        List<Id> libsIds = new ArrayList<>();
        for (int i = 0; i < restJudgementMethodData.getDependencies().size(); i++) {
            libsIds.add(new Id(restJudgementMethodData.getDependencies().get(i)));
        }
        return new FileContent(restJudgementMethodData.getCode(),
                restJudgementMethodData.getName(), restJudgementMethodData.getImports(), libsIds);
    }


    public RestJudgementMethod toDto(JudgementMethodDefinition judgementMethodDefinition) {
        RestJudgementMethod restJudgementMethod = new RestJudgementMethod();
        restJudgementMethod.setId(judgementMethodDefinition.getMethodId().toString());
        restJudgementMethod.setData(toDto(judgementMethodDefinition.getData()));
        return restJudgementMethod;
    }

    public RestJudgementMethodData toDto(FileContent fileContent) {
        RestJudgementMethodData restJudgementMethodData = new RestJudgementMethodData();
        restJudgementMethodData.setName(fileContent.getName());
        restJudgementMethodData.setCode(fileContent.getCode());
        restJudgementMethodData.setImports(fileContent.getImports());
        List<String> libsIdsStrings = new LinkedList<>();
        for (int i = 0; i < fileContent.getLibsIds().size(); i++) {
            libsIdsStrings.add(fileContent.getLibsIds().get(i).toString());
        }
        restJudgementMethodData.setDependencies(libsIdsStrings);
        return restJudgementMethodData;
    }

    public List<RestJudgementMethod> toDto(List<JudgementMethodDefinition> judgementMethodDefinitions) {
        List<RestJudgementMethod> restJudgementMethods = new ArrayList<>();
        for (JudgementMethodDefinition judgementMethodDefinition : judgementMethodDefinitions) {
            restJudgementMethods.add(toDto(judgementMethodDefinition));
        }
        return restJudgementMethods;
    }
}
