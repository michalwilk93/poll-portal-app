package com.portal.rest.converters;

import com.google.common.base.Preconditions;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.PollCreationRequest;
import com.portal.domain.api.PollDefinition;
import com.portal.domain.api.PollEditableProperties;
import com.portal.domain.api.types.*;
import com.portal.rest.dataTransferObjects.RestPollCreationRequest;

import com.portal.rest.dataTransferObjects.RestPollDefinition;
import com.portal.rest.dataTransferObjects.RestPollEditableProperties;
import com.portal.rest.dataTransferObjects.RestShortPollDesc;

import java.util.ArrayList;
import java.util.List;

public class PollConv {
    public PollCreationRequest fromDto(RestPollCreationRequest rp) {
        Preconditions.checkNotNull(rp.getAuthor());

        PollEditableProperties pep = fromDto(rp.getEditableProperties());
        Alternatives alt = new Alternatives(rp.getAlternatives());

        return new PollCreationRequest(pep,
                alt,
                rp.getAuthor(),
                rp.getCriteriaTree(),
                new Descriptions(rp.getAlternativesDescriptions())
        );
    }

    public PollEditableProperties fromDto(RestPollEditableProperties rpep) {
        Inconsistency inconsistency = Inconsistency.valueOf(rpep.getInconsistencyMethod().toUpperCase());
        GradeScale gs = new GradeScale(rpep.getMinGrade(), rpep.getMaxGrade(), rpep.getInterval());
        Threshold threshold = new Threshold(rpep.getInconsistencyThreshold());
        Id judgementId = new Id(rpep.getJudgementMethodId());
        Access access = Access.valueOf(rpep.getAccess().toUpperCase());

        return new PollEditableProperties(
                rpep.getName(),
                rpep.getDescription(),
                inconsistency,
                gs,
                threshold,
                judgementId,
                access
        );
    }

    public RestPollEditableProperties toDto(PollEditableProperties pep) {
        RestPollEditableProperties rpep = new RestPollEditableProperties();
        rpep.setDescription(pep.getDescription());
        rpep.setName(pep.getName());
        rpep.setMaxGrade(pep.getScale().getMax());
        rpep.setMinGrade(pep.getScale().getMin());
        rpep.setInterval(pep.getScale().getInterval());
        rpep.setInconsistencyThreshold(pep.getInconsistencyThreshold().getValue());
        rpep.setInconsistencyMethod(pep.getInconsistencyMethod().toString());
        rpep.setJudgementMethodId(pep.getJudgementMethodId().toString());
        rpep.setAccess(rpep.getAccess());
        return rpep;
    }


    public RestPollDefinition toDto(PollDefinition pd) {
        RestPollDefinition result = new RestPollDefinition();
        RestPollEditableProperties rpep = toDto(pd.getEditableProperties());
        result.setEditableProperties(rpep);
        result.setAlternatives(pd.getAlternatives().getAlternatives());
        result.setAlternativesDescriptions(pd.getAlternativesDescriptions().getAlternativesDescriptions());
        result.setAuthor(pd.getAuthor());
        result.setCriteriaTree(pd.getCriteriaTree());
        result.setUuid(pd.getId().toString());
        return result;
    }

    public RestShortPollDesc toShortDesc(PollDefinition pollDefinition) {
        RestShortPollDesc restShortPollDesc = new RestShortPollDesc();
        restShortPollDesc.setAuthor(pollDefinition.getAuthor());
        restShortPollDesc.setDescription(pollDefinition.getEditableProperties().getDescription());
        restShortPollDesc.setName(pollDefinition.getEditableProperties().getName());
        restShortPollDesc.setUuid(pollDefinition.getId().toString());
        return restShortPollDesc;
    }

    public List<RestPollDefinition> toDto(List<PollDefinition> definitions) {
        List<RestPollDefinition> result = new ArrayList<>();
        for (PollDefinition pd : definitions) {
            result.add(toDto(pd));
        }
        return result;
    }


}
