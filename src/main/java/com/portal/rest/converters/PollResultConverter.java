package com.portal.rest.converters;


import com.portal.domain.api.PollResult;
import com.portal.rest.dataTransferObjects.RestPollResult;

public class PollResultConverter {
    public RestPollResult toDto(PollResult result) {
        RestPollResult restPollResult = new RestPollResult(result.getRoot(), result.getPollId().toString());
        restPollResult.setRespondentCount(result.getRespondentCount());
        return restPollResult;
    }
}
