package com.portal.rest.converters;

import com.portal.domain.api.FilledPoll;
import com.portal.domain.impl.ahp.inputTree.InputNode;
import com.portal.domain.impl.ahp.inputTree.InputBranch;
import com.portal.domain.impl.ahp.inputTree.InputLeaf;
import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.Inconsistency;
import com.portal.rest.dataTransferObjects.RestFilledPoll;
import com.portal.domain.api.types.Node;

import java.util.ArrayList;
import java.util.List;

public class FilledPollConv {
    public FilledPoll fromDto(RestFilledPoll rfp) {
        Id pollId = new Id(rfp.getId());
        Id judgementMethodId = new Id(rfp.getJudgementMethodId());
        Inconsistency inconsistency = Inconsistency.SAATY;//TODO add more inconsistency methods
        InputBranch inRoot = new InputBranch();
        FilledPoll fp = new FilledPoll(pollId, inRoot, judgementMethodId, inconsistency);
        convertNodeToInputBranch(rfp.getRoot(), inRoot, rfp);
        return fp;
    }

    private void convertNodeToInputBranch(Node oldTree, InputBranch inBranch, RestFilledPoll rfp) {
        String nodeName = oldTree.getName();
        inBranch.setName(nodeName);
        double[][] assessmentDoubleMatrix = rfp.getCriteriaPreferences().get(nodeName);
        ComparisonMatrix comparisonMatrix = new ComparisonMatrix(assessmentDoubleMatrix);
        inBranch.setCriteriaPreferences(comparisonMatrix);

        List<InputNode> inChildren = new ArrayList<>();
        for (Node child : oldTree.getSubcriteria()) {
            boolean thisChildIsBranch = child.getSubcriteria() != null && child.getSubcriteria().size() > 0;
            if (thisChildIsBranch) {
                InputBranch inputBranch = new InputBranch();
                convertNodeToInputBranch(child, inputBranch, rfp);
                inChildren.add(inputBranch);
            } else {
                InputLeaf inputLeaf = new InputLeaf();
                convertNodeToInputLeaf(child, inputLeaf, rfp);
                inChildren.add(inputLeaf);
            }
        }
        inBranch.setSubcriteria(inChildren);
    }

    private void convertNodeToInputLeaf(Node oldTree, InputLeaf inLeaf, RestFilledPoll rfp) {
        String nodeName = oldTree.getName();
        inLeaf.setName(nodeName);
        double[][] assessmentDoubleMatrix = rfp.getAlternativesPreferences().get(nodeName);
        ComparisonMatrix comparisonMatrix = new ComparisonMatrix(assessmentDoubleMatrix);
        inLeaf.setAlternativePreferences(comparisonMatrix);
    }
}
