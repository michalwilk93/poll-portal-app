package com.portal.rest.converters;


import com.portal.domain.api.types.Library;
import com.portal.rest.dataTransferObjects.RestLibraryInfo;

import java.util.ArrayList;
import java.util.List;

public class LibraryInfoConv {
    public RestLibraryInfo toDto(Library library) {
        RestLibraryInfo restLibraryInfo = new RestLibraryInfo();
        restLibraryInfo.setId(library.getId().toString());
        restLibraryInfo.setFileName(library.getFileName());
        return restLibraryInfo;
    }

    public List<RestLibraryInfo> toDto(List<Library> userLibraries) {
        List<RestLibraryInfo> result = new ArrayList<>();
        for (Library userLibrary : userLibraries) {
            result.add(toDto(userLibrary));
        }
        return result;
    }
}
