package com.portal.rest.converters;

import com.portal.domain.api.InconsistencyCheckRequest;
import com.portal.domain.api.types.ComparisonMatrix;
import com.portal.domain.api.types.Inconsistency;
import com.portal.domain.api.types.Threshold;
import com.portal.rest.dataTransferObjects.RestInconsistencyCheckRequest;

public class InconsistencyConv {
    public InconsistencyCheckRequest fromDto(RestInconsistencyCheckRequest rirc) {
        ComparisonMatrix cm = new ComparisonMatrix(rirc.getMatrix());
        Inconsistency inconsistency = Inconsistency.valueOf(rirc.getMethod().toUpperCase());
        Threshold threshold = new Threshold(rirc.getThreshold());
        InconsistencyCheckRequest icr = new InconsistencyCheckRequest(cm, threshold, inconsistency);
        return icr;
    }
}
