package com.portal.rest.controllers;

import com.portal.domain.api.AccountMgmt;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.types.AccountStatus;
import com.portal.rest.converters.RestUserConv;
import com.portal.rest.dataTransferObjects.RestAccountInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@RequestMapping(value = "/auth")
@RestController
public class AccountController {
    private final RestUserConv restUserConv;
    private final AccountMgmt accountMgmt;

    @Autowired
    public AccountController(AccountMgmt accountMgmt) {
        this.accountMgmt = accountMgmt;
        this.restUserConv = new RestUserConv();
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public void logout(HttpServletRequest request) {
        for (Cookie c : request.getCookies()) {
            if (c.getName().equals("X-Access-Token")) {
                accountMgmt.logout(c.getValue());
            }
        }
    }

    @RequestMapping(value = "/users", method = RequestMethod.GET)
    public List<RestAccountInfo> getAccountsInfo() throws AuthException {
        return restUserConv.toDto(accountMgmt.getAccountsInfo());
    }

    @RequestMapping(value = "/users/{name:.+}", method = RequestMethod.GET)
    public void changeStatus(@PathVariable(value = "name") String user,
                             @RequestParam String status) throws AuthException {
        accountMgmt.changeAccountStatus(user, AccountStatus.valueOf(status.toUpperCase()));
    }
}
