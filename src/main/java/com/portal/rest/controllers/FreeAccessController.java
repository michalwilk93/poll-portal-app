package com.portal.rest.controllers;

import com.portal.domain.api.*;
import com.portal.domain.api.exceptions.AccountCreationException;
import com.portal.domain.api.exceptions.IpUsedException;
import com.portal.domain.api.exceptions.PollException;
import com.portal.domain.api.exceptions.SignInException;
import com.portal.domain.api.types.*;
import com.portal.rest.converters.*;
import com.portal.rest.dataTransferObjects.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/rest")
public class FreeAccessController {
    private final FilledPollConv filledPollConv;
    private final InconsistencyConv inconsistencyConv;
    private final PollConv pollConv;
    private final PollMgmt pollMgmt;
    private final RestUserConv restUserConv;
    private final AccountMgmt accountMgmt;
    private final PollResultConverter resultConverter;

    @Autowired
    public FreeAccessController(PollMgmt pollMgmt, AccountMgmt accountMgmt) {
        this.pollMgmt = pollMgmt;
        this.accountMgmt = accountMgmt;
        this.filledPollConv = new FilledPollConv();
        this.inconsistencyConv = new InconsistencyConv();
        this.pollConv = new PollConv();
        this.restUserConv = new RestUserConv();
        this.resultConverter = new PollResultConverter();
    }

    @RequestMapping(value = "/public-polls", method = RequestMethod.GET)
    public List<RestShortPollDesc> getPublicPolls() {
        List<PollDefinition> definitions = pollMgmt.getPublicPolls();
        List<RestShortPollDesc> result = new ArrayList<>();
        for (PollDefinition pd : definitions) {
            result.add(pollConv.toShortDesc(pd));
        }
        return result;

    }

    @RequestMapping(value = "/polls", method = RequestMethod.GET)
    public RestPollDefinition getPollFromLink(@RequestParam String uuid, HttpServletRequest request) throws IpUsedException {
        Id id = new Id(uuid);
        String ipAddr = request.getRemoteAddr();
        return pollConv.toDto(pollMgmt.takePoll(id, ipAddr));
    }

    @RequestMapping(value = "/checkInconsistency", method = RequestMethod.POST)
    public boolean checkInconsistency(@RequestBody RestInconsistencyCheckRequest request) {
        InconsistencyCheckRequest irc = inconsistencyConv.fromDto(request);
        return pollMgmt.checkInconsistency(irc);
    }


    @RequestMapping(value = "/results", method = RequestMethod.POST)
    public RestPollResult getResultAndUpdateStatistics(@RequestBody RestFilledPoll res,
                                                       HttpServletRequest request) throws PollException {
        FilledPoll filledPoll = filledPollConv.fromDto(res);
        PollResult result = pollMgmt.createFeedBack(filledPoll, request.getRemoteAddr());
        return resultConverter.toDto(result);
    }

    @RequestMapping(value = "/sign-in", method = RequestMethod.POST)
    public RestRequestResult signInUser(HttpServletRequest request,
                                        @RequestBody RestCredentials rc,
                                        HttpServletResponse res) {
        SignInRequest signInRequest = new SignInRequest(request.getRemoteAddr(), rc.getName(), rc.getPassword());
        RestRequestResult restRequestResult = new RestRequestResult();
        Token token;
        try {
            token = accountMgmt.generateToken(signInRequest);
            res.addCookie(createCookie(token));
            restRequestResult.setMessageCode(getStatusCode(rc.getName()));
            restRequestResult.setSuccessful(true);
        } catch (SignInException sie) {
            restRequestResult.setMessage(sie.getErrorMessage());
            restRequestResult.setSuccessful(false);
        }
        return restRequestResult;
    }

    @RequestMapping(value = "/rest/users", method = RequestMethod.POST)
    public void createUser(@RequestBody RestCredentials restCredentials) throws AccountCreationException {
        UserCredentials userCredentials = restUserConv.fromDto(restCredentials);
        accountMgmt.createAccount(userCredentials);
    }

    private int getStatusCode(String userName) {
        AccountStatus status = accountMgmt.getAccountStatus(userName);
        if (status == AccountStatus.ROOT) {
            return 223;
        } else if (status == AccountStatus.ADMIN) {
            return 222;
        } else {
            return 221;
        }
    }

    private Cookie createCookie(Token token) {
        Cookie cookie = new Cookie("X-Access-Token", token.getTokenValue());
        cookie.setMaxAge(60 * 60 * 24);
        cookie.setPath("/auth");
        return cookie;
    }
}
