package com.portal.rest.controllers;

import com.portal.domain.api.*;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.PollCreationException;
import com.portal.domain.api.types.Id;
import com.portal.rest.converters.*;
import com.portal.rest.dataTransferObjects.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(value = "/auth/polls")
@RestController
public class PollController {
    private final FilledPollConv filledPollConv;
    private final PollConv pollConv;
    private final PollMgmt pollMgmt;

    @Autowired
    public PollController(PollMgmt pollMgmt) {
        this.filledPollConv = new FilledPollConv();
        this.pollConv = new PollConv();
        this.pollMgmt = pollMgmt;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<RestPollDefinition> getPollsCreatedBy(@RequestParam(value = "author", required = false) String author) {
        List<PollDefinition> definitions;
        if (author != null) {
            definitions = pollMgmt.getUserPolls(author);
        } else {
            definitions = pollMgmt.getCurrentUserPolls();
        }
        return pollConv.toDto(definitions);
    }


    @RequestMapping(value = "/{id}", method = RequestMethod.PATCH)
    public void updatePoll(@PathVariable(value = "id") String uuid,
                           @RequestBody RestPollEditableProperties editableProperties) throws PollCreationException {
        PollEditableProperties pep = pollConv.fromDto(editableProperties);
        Id editedPollId = new Id(uuid);
        pollMgmt.updatePoll(editedPollId, pep);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePoll(@PathVariable(value = "id") String pollId) throws AuthException {
        Id pollToRemoveId = new Id(pollId);
        pollMgmt.deletePoll(pollToRemoveId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void createPoll(@RequestBody RestPollCreationRequest restPollCreationRequest) throws PollCreationException {
        PollCreationRequest pcRequest = pollConv.fromDto(restPollCreationRequest);
        pollMgmt.createNewPoll(pcRequest);
    }
}



