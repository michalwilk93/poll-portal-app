package com.portal.rest.controllers;

import com.portal.domain.api.JudgementMethodDefinition;
import com.portal.domain.api.JudgementMethodMgmt;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.exceptions.CompilationException;
import com.portal.domain.api.types.Id;
import com.portal.domain.api.types.FileContent;
import com.portal.rest.converters.*;
import com.portal.rest.dataTransferObjects.RestJudgementMethod;
import com.portal.rest.dataTransferObjects.RestJudgementMethodData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping(value = "/auth/judgement")
public class JudgementMethodController {
    private final JudgementRestConv judgementRestConv;
    private final JudgementMethodMgmt judgementMethodMgmt;

    @Autowired
    public JudgementMethodController(JudgementMethodMgmt judgementMethodMgmt) {
        this.judgementMethodMgmt = judgementMethodMgmt;
        this.judgementRestConv = new JudgementRestConv();
    }

    @RequestMapping(method = RequestMethod.POST)
    public void createCustomJudgementMethod(@RequestBody RestJudgementMethodData data)
            throws AuthException, CompilationException {
        FileContent method = judgementRestConv.fromDto(data);
        judgementMethodMgmt.createJudgementMethod(method);
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<RestJudgementMethod> getCustomJudgementMethods() throws AuthException {
        List<JudgementMethodDefinition> definitions = judgementMethodMgmt.getCustomJudgementMethods();
        return judgementRestConv.toDto(definitions);
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.PATCH)
    public void editJudgementMethod(@PathVariable(value = "uuid") String uuid,
                                    @RequestBody RestJudgementMethodData data) throws AuthException {
        FileContent method = judgementRestConv.fromDto(data);
        judgementMethodMgmt.editJudgementMethod(new Id(uuid), method);
    }
}
