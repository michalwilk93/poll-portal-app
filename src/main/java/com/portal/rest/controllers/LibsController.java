package com.portal.rest.controllers;

import com.portal.domain.api.JudgementMethodMgmt;
import com.portal.domain.api.exceptions.AuthException;
import com.portal.domain.api.types.Library;
import com.portal.rest.converters.LibraryInfoConv;
import com.portal.rest.dataTransferObjects.RestLibraryInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

@RequestMapping(value = "/auth/libraries")
@RestController
public class LibsController {
    private final LibraryInfoConv libraryInfoConv;
    private final JudgementMethodMgmt judgementMethodMgmt;

    @Autowired
    public LibsController(JudgementMethodMgmt judgementMethodMgmt) {
        this.libraryInfoConv = new LibraryInfoConv();
        this.judgementMethodMgmt = judgementMethodMgmt;
    }

    @RequestMapping(method = RequestMethod.POST)
    public void uploadFile(MultipartHttpServletRequest request) throws IOException, AuthException {
        Iterator<String> itr = request.getFileNames();
        MultipartFile file = request.getFile(itr.next());
        judgementMethodMgmt.addLibrary(file.getBytes(), file.getOriginalFilename());
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<RestLibraryInfo> getUploadedLibraries() throws AuthException {
        List<Library> userLibraries = judgementMethodMgmt.getLibraries();
        return libraryInfoConv.toDto(userLibraries);
    }
}
