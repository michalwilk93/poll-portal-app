package com.portal.rest.controllers;

import com.portal.domain.api.PollMgmt;
import com.portal.domain.api.PollResult;
import com.portal.domain.api.types.Id;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RequestMapping(value = "/auth/statistics")
@RestController
public class StatisticsController {
    private final PollMgmt pollMgmt;

    @Autowired
    public StatisticsController(PollMgmt pollMgmt) {
        this.pollMgmt = pollMgmt;
    }

    @RequestMapping(value = "/{uuid}", method = RequestMethod.GET)
    public PollResult getPollStatistics(@RequestParam String uuid) {
        return pollMgmt.getGeneralStatistics(new Id(uuid));
    }
}
