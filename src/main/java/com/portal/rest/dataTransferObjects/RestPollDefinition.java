package com.portal.rest.dataTransferObjects;

import com.portal.domain.api.types.Node;

import java.util.List;
import java.util.Map;

public class RestPollDefinition {
    private RestPollEditableProperties editableProperties;
    private String author;
    private String uuid;
    private Node criteriaTree;
    private Map<String, Map<String, String>> alternativesDescriptions;
    private List<String> alternatives;

    public RestPollEditableProperties getEditableProperties() {
        return editableProperties;
    }

    public void setEditableProperties(RestPollEditableProperties editableProperties) {
        this.editableProperties = editableProperties;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<String> alternatives) {
        this.alternatives = alternatives;
    }

    public Map<String, Map<String, String>> getAlternativesDescriptions() {
        return alternativesDescriptions;
    }

    public void setAlternativesDescriptions(Map<String, Map<String, String>> alternativesDescriptions) {
        this.alternativesDescriptions = alternativesDescriptions;
    }

    public Node getCriteriaTree() {
        return criteriaTree;
    }

    public void setCriteriaTree(Node criteriaTree) {
        this.criteriaTree = criteriaTree;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
