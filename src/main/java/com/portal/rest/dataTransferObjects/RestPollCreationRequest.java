package com.portal.rest.dataTransferObjects;

import com.portal.domain.api.types.Node;

import java.util.*;

public class RestPollCreationRequest  {
    private RestPollEditableProperties editableProperties;
    private List<String> alternatives;
    private Node criteriaTree;
    private String author;
    private Map<String, Map<String, String>> alternativesDescriptions;

    public RestPollEditableProperties getEditableProperties() {
        return editableProperties;
    }

    public void setEditableProperties(RestPollEditableProperties editableProperties) {
        this.editableProperties = editableProperties;
    }

    public List<String> getAlternatives() {
        return alternatives;
    }

    public void setAlternatives(List<String> alternatives) {
        this.alternatives = alternatives;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public Node getCriteriaTree() {
        return criteriaTree;
    }

    public void setCriteriaTree(Node criteriaTree) {
        this.criteriaTree = criteriaTree;
    }

    public Map<String, Map<String, String>> getAlternativesDescriptions() {
        return alternativesDescriptions;
    }

    public void setAlternativesDescriptions(Map<String, Map<String, String>> alternativesDescriptions) {
        this.alternativesDescriptions = alternativesDescriptions;
    }
}
