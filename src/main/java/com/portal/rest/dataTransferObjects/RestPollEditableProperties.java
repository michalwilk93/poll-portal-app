package com.portal.rest.dataTransferObjects;


public class RestPollEditableProperties {
    private String name;
    private String description;
    private String inconsistencyMethod;
    private double minGrade;
    private double maxGrade;
    private double interval;
    private String access;
    private String judgementMethodId;
    private double inconsistencyThreshold;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getInconsistencyMethod() {
        return inconsistencyMethod;
    }

    public void setInconsistencyMethod(String inconsistencyMethod) {
        this.inconsistencyMethod = inconsistencyMethod;
    }

    public double getMinGrade() {
        return minGrade;
    }

    public void setMinGrade(double minGrade) {
        this.minGrade = minGrade;
    }

    public double getMaxGrade() {
        return maxGrade;
    }

    public void setMaxGrade(double maxGrade) {
        this.maxGrade = maxGrade;
    }

    public double getInterval() {
        return interval;
    }

    public void setInterval(double interval) {
        this.interval = interval;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public double getInconsistencyThreshold() {
        return inconsistencyThreshold;
    }

    public void setInconsistencyThreshold(double inconsistencyThreshold) {
        this.inconsistencyThreshold = inconsistencyThreshold;
    }

    public String getJudgementMethodId() {
        return judgementMethodId;
    }

    public void setJudgementMethodId(String judgementMethodId) {
        this.judgementMethodId = judgementMethodId;
    }
}
