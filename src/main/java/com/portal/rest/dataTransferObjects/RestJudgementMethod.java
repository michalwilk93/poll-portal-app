package com.portal.rest.dataTransferObjects;


public class RestJudgementMethod {
    private RestJudgementMethodData data;
    private String id;

    public RestJudgementMethodData getData() {
        return data;
    }

    public void setData(RestJudgementMethodData data) {
        this.data = data;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
