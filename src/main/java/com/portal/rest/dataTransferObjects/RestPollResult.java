package com.portal.rest.dataTransferObjects;

import com.portal.domain.impl.ahp.outputTree.OutputBranch;

public class RestPollResult {
    private final OutputBranch root;
    private final String pollId;
    private int respondentCount;

    public RestPollResult(OutputBranch root, String pollId) {
        this.root = root;
        this.pollId = pollId;
    }

    public void setRespondentCount(int respondentCount) {
        this.respondentCount = respondentCount;
    }

    public String getPollId() {
        return pollId;
    }

    public OutputBranch getRoot() {
        return root;
    }

    public int getRespondentCount() {
        return respondentCount;
    }
}
