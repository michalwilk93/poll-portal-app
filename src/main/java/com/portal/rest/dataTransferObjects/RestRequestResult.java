package com.portal.rest.dataTransferObjects;

import java.util.List;

public class RestRequestResult {
    private boolean successful;
    private int messageCode;
    private List<Object> errorDetails;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccessful() {
        return successful;
    }

    public void setSuccessful(boolean successful) {
        this.successful = successful;
    }

    public int getMessageCode() {
        return messageCode;
    }

    public void setMessageCode(int messageCode) {
        this.messageCode = messageCode;
    }

    public List<Object> getErrorDetails() {
        return errorDetails;
    }

    public void setErrorDetails(List<Object> errorDetails) {
        this.errorDetails = errorDetails;
    }
// messageCode == DUPLICATE_NAME -> errorDetails = [what name]
}