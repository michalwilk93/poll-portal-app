package com.portal.rest.dataTransferObjects;

import com.portal.domain.api.types.Node;

import java.util.Map;

public class RestFilledPoll {
    private String id;
    private String judgementMethodId;
    private String inconsistency;
    private Node root;
    private Map<String, double[][]> alternativesPreferences;
    private Map<String, double[][]> criteriaPreferences;

    public String getJudgementMethodId() {
        return judgementMethodId;
    }

    public void setJudgementMethodId(String judgementMethodId) {
        this.judgementMethodId = judgementMethodId;
    }

    public String getInconsistency() {
        return inconsistency;
    }

    public void setInconsistency(String inconsistency) {
        this.inconsistency = inconsistency;
    }

    public Map<String, double[][]> getAlternativesPreferences() {
        return alternativesPreferences;
    }

    public void setAlternativesPreferences(Map<String, double[][]> alternativesPreferences) {
        this.alternativesPreferences = alternativesPreferences;
    }

    public Map<String, double[][]> getCriteriaPreferences() {
        return criteriaPreferences;
    }

    public void setCriteriaPreferences(Map<String, double[][]> criteriaPreferences) {
        this.criteriaPreferences = criteriaPreferences;
    }

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
