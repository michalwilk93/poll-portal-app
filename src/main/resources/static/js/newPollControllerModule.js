var treeControllerModule = angular.module('newPollControllerModule', []);
treeControllerModule.controller("newPollController", ['$scope', '$rootScope', '$http',
    function ($scope, $rootScope, $http) {
    $scope.getMethods = function () {
        $http.get('/auth/judgement').success(function (data) {
            if (data.successful === false) {
                $rootScope.err = data;
                $location.url("/err");
                console.log("Get judgement methods unsuccessfull");
            } else {
                console.log(data);
                console.log("Get judgement methods successfull");
                $scope.customMethods = data;
            }
        }).error(function (data) {
            console.log("Get judgement methods unsuccesfull");
        });
    };
    $scope.getMethods();
    console.log("newPoll controller");
    $scope.criterionName = "";
    $scope.newPoll = {};
    $scope.newPoll.alternatives = [];
    $scope.newPoll.author = $rootScope.loggedUser;
    $scope.newPoll.editableProperties = {};
    $scope.newPoll.editableProperties.name = "Default name";
    $scope.newPoll.editableProperties.description = "Default description";
    $scope.newPoll.editableProperties.inconsistencyMethod = "Saaty";
    $scope.newPoll.editableProperties.inconsistencyThreshold = 0.1;
    $scope.newPoll.editableProperties.interval = 1;
    $rootScope.newPollOK =false;
    $scope.gradeScaleOK = true;
    $scope.showGradeScaleErr = false;
    $scope.addAlternative = function () {
        $scope.newPoll.alternatives.push("");
    };
    $scope.deleteAlternative = function (ind) {
        $scope.newPoll.alternatives.splice(ind, 1);
    };
    $scope.generatedTable = false;
    $scope.treeInList = [{
        name: "root",
        subcriteria: []
    }];
    $scope.delete = function (data) {
        data.subcriteria = [];
    };
    $scope.add = function (data, name) {
        var newName = name;
        data.subcriteria.push({name: newName, subcriteria: []});
        console.log($scope.treeInList[0]);
        $scope.criterionName = "";
    };
    $scope.rangeSliderValue = "1;9";
    $scope.pollCreationSlidersOptions = {
        from: 1,
        to: 50,
        step: 1,
        dimension: "",
        css: {
            background: {"background-color": "silver"},
            before: {"background-color": "green"},
            default: {"background-color": "green"},
            after: {"background-color": "green"},
            pointer: {"background-color": "red"}
        }
    };
    $scope.generateTable = function () {
        console.log($scope.newPoll.alternatives);
        $scope.alternativesMap = $scope.generateAlternativesMap($scope.newPoll.alternatives);
        console.log($scope.alternativesMap);
        $scope.generatedTable = true;
    };
    $scope.findLeafs = function (nodeList) {
        console.log(nodeList);
        var result = [];
        for (var index = 0; index < nodeList.length; ++index) {

            if (nodeList[index].subcriteria.length == 0) {
                result.push(nodeList[index].name);
            } else {
                var helpList = $scope.findLeafs(nodeList[index].subcriteria);
                result = result.concat(helpList);//because concat creates new array!
            }
        }
        return result;
    };
    $scope.generateAlternativesMap = function (alternativesList) {
        $scope.nodesWithoutChildren = $scope.findLeafs($scope.treeInList[0].subcriteria);
        //sizeT to jest zmienna sluzaca  do wysw tabeli opisow
        $scope.sizeT = $scope.nodesWithoutChildren.length * 50;
        console.log($scope.nodesWithoutChildren);
        var result = {};
        for (var i = 0; i < alternativesList.length; i++) {
            var map = {};
            for (var k = 0; k < $scope.nodesWithoutChildren.length; k++) {
                map[$scope.nodesWithoutChildren[k]] = "";
            }
            result[alternativesList[i]] = map;
        }
        return result;
    };

    //end of 1st phase=====================================================================

    $scope.createNewPoll = function () {
        $scope.newPoll.alternativesDescriptions = $scope.alternativesMap;
        //todo saaty i koczkodaj
        $scope.newPoll.editableProperties.inconsistencyMethod = "Saaty";
        $scope.newPoll.editableProperties.judgementMethodId;
        $scope.newPoll.criteriaTree = $scope.treeInList[0];
        var range = $scope.rangeSliderValue.split(";");
        $scope.newPoll.editableProperties.minGrade = parseFloat(range[0]);
        $scope.newPoll.editableProperties.maxGrade = parseFloat(range[1]);
        console.log($scope.newPoll);
        $http.post('/auth/polls/', $scope.newPoll).success(function (data) {
            if (data.successful === false) {
                console.log("Creating new poll failed");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                $rootScope.newPollOK = true;
                console.log("Creating new poll created successful!");
            }
        }).error(function (data) {
            console.log("Creating new poll failed");
            $rootScope.newPollOK = false;
        });
    };

    $scope.checkGradeScale = function () {
        var range = $scope.rangeSliderValue.split(";");
        var min = parseFloat(range[0]);
        var max = parseFloat(range[1]);
        if ((max - min) % $scope.newPoll.editableProperties.interval == 0) {
            $scope.gradeScaleOK = true;
            $scope.showGradeScaleErr = false;
        } else {
            $scope.gradeScaleOK = false;
            $scope.showGradeScaleErr = true;
        }
    };
}]);