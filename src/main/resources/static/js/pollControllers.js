var pollControllers = angular.module('pollControllers', []);

pollControllers.controller('publicPollsController', function ($rootScope, $scope, $http, $location) {
    $scope.elementsInARow = 4;
    var sgIndex = 0;
    $scope.publicPollsArrayGroups = [];
    $scope.publicPollsArrayGroups[sgIndex] = [];
    $http.get('/rest/public-polls').success(function (data) {
        if(data.successful === false){
            $location.url("/err");
            $rootScope.err = data;
            console.log("Get public polls failed");
        }else{
            console.log(data);
            $scope.publicPollsList = data;
            console.log("Get public polls succeeded");
            for (var i = 0; i < $scope.publicPollsList.length; i++) {
                var p = $scope.publicPollsList[i];
                $scope.publicPollsArrayGroups[sgIndex].push(p);
                if ($scope.publicPollsArrayGroups[sgIndex].length == $scope.elementsInARow) {
                    $scope.publicPollsArrayGroups[++sgIndex] = [];
                }

            }
        }
    }).error(function () {
        console.log("Get public polls failed");
    });

    $scope.takePoll = function (pollDesc) {
        $http.get('/rest/getPoll?uuid=' + pollDesc.uuid).success(function (data) {
            console.log(data);
            if(data.successful === false){
                console.log("Cant get poll with link=" + link);
                $location.url("/err");
                $rootScope.err = data;
            }else{
                $rootScope.currentPoll = data;
                $location.path("/pollTaking");
                $location.url($location.path()); //in order to clear link= because it stays
            }
        }).error(function () {
            console.log("Cant get poll with link=" + link);
        });
    };
});

pollControllers.controller('myPollsController', function ($location, $rootScope, $scope, $http) {

    $scope.getPolls = function(){
        $scope.elementsInARow = 4;
        var sgIndex = 0;
        $scope.userPollsArrayGroups = [];
        $scope.userPollsArrayGroups[sgIndex] = [];

        $http.get('/auth/polls?author='+$rootScope.loggedUser).success(function (data) {
            if(data.successful === false){
                $location.url("/err");
                $rootScope.err = data;
                console.log("Get public polls failed");
            }else{
                $scope.pollsCreatedByUser = data;
                console.log("Get user polls succeeded");
                for (var i = 0; i < $scope.pollsCreatedByUser.length; i++) {
                    var p = $scope.pollsCreatedByUser[i];
                    $scope.userPollsArrayGroups[sgIndex].push(p);
                    if ($scope.userPollsArrayGroups[sgIndex].length == $scope.elementsInARow) {
                        $scope.userPollsArrayGroups[++sgIndex] = [];
                    }
                }
            }
        }).error(function () {
            console.log("Get user polls failed");
        });
    };
    $scope.getPolls();
    $scope.takePoll = function (poll) {
        $rootScope.currentPoll = poll;
        $location.path("/pollTaking");
    };
    $scope.seeStatistics = function (poll) {
        $http.get('/auth/statistics?uuid=' + poll.uuid).success(function (data) {
            if (data) {
                if(data.successful === false){
                    $location.url("/err");
                    $rootScope.err = data;
                    console.log("Get statistics failed");
                }else{
                    $rootScope.currentPoll = poll;
                    $rootScope.pollResult = data;
                    console.log("Got statistics  with id=" + poll.uuid);
                    $location.path("/statistics");
                }
            } else {
                $scope.noStatisticsAlert = true;
            }
        }).error(function () {
            console.log("Cant get statistics  with id=" + poll.uuid);
        });
    };

    $scope.deletePoll = function (uuid) {
        $http.delete('/auth/polls/' + uuid).success(function (data) {
            if (data.successful === false) {
                console.log("Deleting poll failed");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                console.log("Poll deleted");
                $scope.getPolls();
            }
        }).error(function () {
            console.log("Delete poll failed");
        });
    }

});

pollControllers.controller('getPollFromLinkController', function ($location, $rootScope, $scope, $http, $routeParams) {
    var link = $routeParams.link;
    $http.get('/rest/getPoll?uuid=' + link).success(function (data) {
        if(data.successful === false){
            $rootScope.err = data;
            $location.path("/err");
            $location.url($location.path());
        }else{
            $rootScope.currentPoll = data;
            $location.path("/pollTaking");
            $location.url($location.path()); //in order to clear link= because it stays
        }
    }).error(function () {
        console.log("Cant get poll with link=" + link);
    });
});

