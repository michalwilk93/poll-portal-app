var pollTakingControllerModule = angular.module('pollTakingControllerModule', []);

pollTakingControllerModule.controller('pollTakingController', function ($rootScope, $scope, $http, $location) {
    console.log($rootScope.currentPoll);
    $scope.secondPhase = false;
    $scope.showingFeedback = false;
    $scope.showInconsistencyError = false;
    $scope.allocateMatrix = function (size) {
        var res = new Array(size);
        for (var i = 0; i < size; i++) {
            res[i] = new Array(size);
            res[i][i] = 1;
        }
        return res;
    };

    $scope.options = {
        from: (-($rootScope.currentPoll.editableProperties.maxGrade) + 1),
        to: ($rootScope.currentPoll.editableProperties.maxGrade - 1),
        step: $rootScope.currentPoll.editableProperties.interval,
        modelLabels: function (value) {
            if (value == 0) {
                return "1:1";
            } else if (value < 0) {
                return (Math.abs(value) + 1) + ":1";
            } else {
                return "1:" + (1 + value);
            }
        },
        dimension: "",
        css: {
            background: {"background-color": "silver"},
            before: {"background-color": "green"},
            default: {"background-color": "green"},
            after: {"background-color": "green"},
            pointer: {"background-color": "red"}
        }
    };
    $scope.findLeafs = function (nodeList) {
        console.log("findLeafs");
        console.log(nodeList);
        var result = [];
        for (var index = 0; index < nodeList.length; ++index) {
            if (nodeList[index].subcriteria.length == 0) {
                result.push(nodeList[index].name);
            } else {
                var helpList = $scope.findLeafs(nodeList[index].subcriteria);
                result = result.concat(helpList);//because concat creates new array!
            }
        }
        return result;
    };
    $scope.comparisonCriteria = $scope.findLeafs($rootScope.currentPoll.criteriaTree.subcriteria);
    $scope.generateMapLeaf2Matrix = function () {
        var size = $rootScope.currentPoll.alternatives.length;
        var resultMap = {};
        for (var i = 0; i < $scope.comparisonCriteria.length; i++) {
            var arr2d = $scope.allocateMatrix(size);
            for (var alternative1stDim = 0; alternative1stDim < size; alternative1stDim++) {
                for (var alternative2ndDim = 0; alternative2ndDim < size; alternative2ndDim++) {
                    arr2d[alternative1stDim][alternative2ndDim] = 0;
                }
            }
            resultMap[$scope.comparisonCriteria[i]] = arr2d;
        }
        return resultMap;
    };
    $scope.mapLeaf2Matrix = $scope.generateMapLeaf2Matrix();


    $scope.addPrefTable = function (arg, dim) {
        var x = new Array(dim);
        for (var i = 0; i < dim; i++) {
            x[i] = new Array(dim);
        }
        for (var w = 0; w < dim; w++) {
            for (var k = 0; k < dim; k++) {
                x[w][k] = 0;
            }
        }
        arg.preferences = x;
    };

    $scope.helper = 1;
    $scope.counter = 0;
    $scope.counter2 = -1;
    $scope.criterionToCounterMap = {};

    $scope.buildTableTree = function (criteriaList) {
        var tableTreeElement = {
            preferences: null,
            subcriteria: [],
            name: null
        };
        $scope.addPrefTable(tableTreeElement, criteriaList.length);
        for (var index = 0; index < criteriaList.length; index++) {
            if (criteriaList[index].subcriteria.length != 0) {
                $scope.criterionToCounterMap[criteriaList[index].name] = $scope.helper;
                $scope.helper = $scope.helper + 1;

                var n = $scope.buildTableTree(criteriaList[index].subcriteria);
                n.name = criteriaList[index].name;
                tableTreeElement.subcriteria.push(n);
            }
        }
        return tableTreeElement;
    };
    $scope.resultTreeInList = [$scope.buildTableTree($rootScope.currentPoll.criteriaTree.subcriteria)];//todo wymag listy a my dajemy jeden
    $scope.resultTreeInList[0].name = "root";

    $scope.criterionToCounterMap["root"] = 0;

    console.log($scope.resultTreeInList);

    $scope.completeTable = function (t) {
        var res = new Array(t.length);
        for (var i = 0; i < t.length; i++) {
            res[i] = new Array(t.length);
        }
        //1's on the diagonal
        for (var w = 0; w < t.length; w++) {
            res[w][w] = 1;
        }

        for (var w = 0; w < t.length; w++) {
            for (var k = w + 1; k < t.length; k++) {
                if (t[w][k] == 0) {
                    res[w][k] = 1;
                    res[k][w] = 1;
                } else if (t[w][k] > 0) {
                    res[w][k] = 1 / ( Math.abs(t[w][k]) + 1);
                    res[k][w] = Math.abs(t[w][k]) + 1;
                } else {
                    res[w][k] = Math.abs(t[w][k]) + 1;
                    res[k][w] = 1 / (Math.abs(t[w][k]) + 1);
                }
            }
        }

        return res;
    };

    //takes name of the criterion we want to show
    $scope.timeToShow = function (n) {
        return ($scope.criterionToCounterMap[n] == $scope.counter);
    };

    $scope.check = function (prefTable) {
        if (prefTable.length > 2) {
            var completed = $scope.completeTable(prefTable);
            console.log(completed);
            console.log("checking table and incrementing counter");
            console.log($scope.counter);
            var tat = {
                matrix: completed,
                threshold: $rootScope.currentPoll.editableProperties.inconsistencyThreshold,
                method: $rootScope.currentPoll.editableProperties.inconsistencyMethod
            };
            $http.post('/rest/checkInconsistency', tat).success(function (data) {
                if (data.successful === false) {
                    console.log("error while checking consistency");
                    $rootScope.err = data;
                    $location.url("/err");
                } else {
                    if (data == true) {
                        $scope.counter = $scope.counter + 1;
                        console.log("table consistent");
                        $scope.showInconsistencyError = false;
                    } else {
                        console.log("table inconsistent");
                        $scope.showInconsistencyError = true;
                    }
                }
            }).error(function (data) {
                console.log("Sending data failed");
            });
        } else {
            $scope.counter = $scope.counter + 1;
            console.log("Table consistent trivially");
        }


    };
    $scope.check2 = function (prefTable) {
        console.log(prefTable);
        if (prefTable.length > 2) {
            console.log("checking the table and incrementing counter2");
            console.log($scope.counter2);
            var completed = $scope.completeTable(prefTable);
            var tat = {
                matrix: completed,
                threshold: $rootScope.currentPoll.editableProperties.inconsistencyThreshold,
                method: $rootScope.currentPoll.editableProperties.inconsistencyMethod
            };
            $http.post('/rest/checkInconsistency', tat).success(function (data) {
                if (data.successful === false) {
                    console.log("error while checking consistency");
                    $rootScope.err = data;
                    $location.url("/err");
                } else {
                    if (data == true) {
                        $scope.counter2 = $scope.counter2 + 1;
                        console.log("Table consistent");
                        $scope.showInconsistencyError = false;
                    } else {
                        console.log("Table inconsistent");
                        $scope.showInconsistencyError = true;
                    }
                }
            }).error(function (data) {
                console.log("Sending data failed");
            });
        } else {
            $scope.counter2 = $scope.counter2 + 1;
            console.log("Table consistent trivially");
        }
    };

    $scope.findElementWithName = function (name, subcriteriaList) {
        for (var index = 0; index < subcriteriaList.length; ++index) {
            if (subcriteriaList[index].name == name) {
                return subcriteriaList[index];
            }
        }
        for (var index2 = 0; index2 < subcriteriaList.length; ++index2) {
            if (subcriteriaList[index2].subcriteria != null) {
                return $scope.findElementWithName(name, subcriteriaList[index2].subcriteria);
            }
        }
    };
    console.log($rootScope.currentPoll.criteriaTree);
    $scope.getSubcriteriaLength = function (nodeName) {
        var x = $scope.findElementWithName(nodeName, [$rootScope.currentPoll.criteriaTree]);
        return x.subcriteria.length;
    };

    $scope.myRange = function (a, b, step) {
        var A = [];
        if (typeof a == 'number') {
            A[0] = a;
            step = step || 1;
            while (a + step <= b) {
                A[A.length] = a += step;
            }
        }
        return A;
    };

    $scope.recomputeAssesment = function (map2darr) {
        console.log(map2darr);
        console.log($scope.comparisonCriteria);
        for (var crit in map2darr) {
            if (map2darr[crit] != undefined) {
                console.log(crit + " -> " + map2darr[crit]);
                var res = $scope.allocateMatrix(map2darr[crit].length);
                for (var w = 0; w < map2darr[crit].length; w++) {
                    for (var k = w + 1; k < map2darr[crit][w].length; k++) {
                        if (map2darr[crit][w][k] == 0) {
                            console.log(crit);
                            res[w][k] = 1;
                            res[k][w] = 1;
                        } else if (map2darr[crit][w][k] > 0) {
                            res[w][k] = ( 1 / ( Math.abs(map2darr[crit][w][k]) + 1));
                            res[k][w] = ( Math.abs(map2darr[crit][w][k]) + 1);
                        } else {
                            res[w][k] = Math.abs(map2darr[crit][w][k]) + 1;
                            res[k][w] = (1 / (Math.abs(map2darr[crit][w][k]) + 1));
                        }
                    }
                }
                map2darr[crit] = res;
                console.log(map2darr[crit]);
            }
        }
    };

    $scope.criteriaPreferences = {};
    $scope.addKeyToTreeToMap = function (root) {
        $scope.criteriaPreferences[root.name] = root.preferences;
        console.log(root.name);
        console.log(root.subcriteria);
        if (root.subcriteria) {
            for (var index = 0; index < root.subcriteria.length; index++) {
                $scope.addKeyToTreeToMap(root.subcriteria[index]);
            }
        }
    };

    $scope.sendResults = function () {
        console.log($scope.mapLeaf2Matrix);
        console.log($scope.resultTreeInList[0]);
        $scope.recomputeAssesment($scope.mapLeaf2Matrix);
        console.log("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        console.log($scope.resultTreeInList[0]);
        $scope.addKeyToTreeToMap($scope.resultTreeInList[0]);
        console.log("-------------------------------------------------------------");
        console.log($scope.criteriaPreferences);
        $scope.recomputeAssesment($scope.criteriaPreferences);
        var filledPoll = {
            id: $rootScope.currentPoll.uuid,
            root: $rootScope.currentPoll.criteriaTree,
            judgement: $rootScope.currentPoll.editableProperties.judgementMethod,
            alternativesPreferences: $scope.mapLeaf2Matrix,
            criteriaPreferences: $scope.criteriaPreferences,
            judgementMethodId: $rootScope.currentPoll.editableProperties.judgementMethodId
        };
        console.log(filledPoll);
        $http.post('/rest/results', filledPoll).success(function (data) {
            if (data.successful === false) {
                console.log("error while sending filled poll");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                console.log("Sending data successful");
                $scope.showingFeedback = true;
                $scope.feedBack = data;
                console.log(data);
                var max = $scope.findMax($scope.feedBack.root.vector);
                console.log($scope.feedBack.root.vector);
                $scope.feedBack.winnerAlternativeIndexes = $scope.findOccurrencesOf($scope.feedBack.root.vector, max);
                console.log($scope.feedBack.winnerAlternativeIndexes);
                console.log(max);
            }
        }).error(function (data) {
            console.log("Sending data failed");
        });
    };


    $scope.redirectToStatistics = function () {
        $rootScope.pollResult = $scope.feedBack;
        $location.path('/statistics');
    };

    $scope.findMax = function (arr) {
        if (arr.length === 0) {
            return -1;
        }
        var maxIndex = 0;

        for (var i = 1; i < arr.length; i++) {
            if (arr[i] > arr[maxIndex]) {
                maxIndex = i;
            }
        }
        return arr[maxIndex];
    };


    $scope.findOccurrencesOf = function (arr, elem) {
        if (arr.length === 0) {
            return -1;
        }
        var result = [];
        for (var i = 0; i < arr.length; i++) {
            if (arr[i] == elem) {
                result.push(i);
            }
        }
        return result;
    }
});