var customJudgementMdl = angular.module('customJudgementMdl', []);
customJudgementMdl.controller('customJudgementController', function ($rootScope, $scope, $http, $location) {
    $scope.getLibraries = function () {
        $http.get('/auth/libraries').success(function (data) {
            if(data.successful === false){
                $rootScope.err = data;
                $location.url("/err");
            }else{
                console.log(data);
                console.log("Get judgement methods successfull");
                $scope.libraries = data;
            }
        }).error(function (data) {
            console.log("Get judgement methods unsuccesfull");
        });
    };

    $scope.getLibraries();
    $scope.customMethod = {
        name: "Please give name to this method",
        code: "Your code here",
        imports:"",
        dependencies:[]
    };
    //http://stackoverflow.com/questions/14514461/how-to-bind-to-list-of-checkbox-values-with-angularjs
    $scope.toggleSelection = function toggleSelection(libID) {
        var idx = $scope.customMethod.dependencies.indexOf(libID);
        // is currently selected
        if (idx > -1) {
            $scope.customMethod.dependencies.splice(idx, 1);
        }
        // is newly selected
        else {
            $scope.customMethod.dependencies.push(libID);
        }
    };


    $scope.editMode = false;
    $scope.getMethods = function () {
        $http.get('/auth/judgement').success(function (data) {
            if(data.successful === false){
                $rootScope.err = data;
                $location.url("/err");
            } else {
                console.log(data);
                console.log("Get judgement methods successfull");
                $scope.customMethods = data;
            }
        }).error(function (data) {
            console.log("Get judgement methods unsuccesfull");
        });
    };

    $scope.getMethods();
    $scope.editMethod = function (method) {
        $scope.editMode = true;
        $scope.customMethod.name = method.data.name;
        $scope.customMethod.code = method.data.code;
        $scope.customMethod.imports = method.data.imports;
        $scope.customMethod.dependencies = method.data.dependencies;
        $scope.currentEditingUuid = method.id;
    };

    $scope.exitEditMode = function(){
        $scope.customMethod = {
            name: "Please give name to this method",
            code: "Your code here",
            imports:"",
            dependencies:[]
        };
        $scope.editMode = false;
    };


    $scope.saveChanges = function () {
        console.log($scope.currentEditingUuid);
        console.log($scope.customMethod);
        $http.post('/auth/judgement/' + $scope.currentEditingUuid, $scope.customMethod).success(function (data) {
            if(data.successful === false){
                $rootScope.err = data;
                $location.url("/err");
            }
        }).error(function (data) {
            console.log("Judgement methods updated unsuccessfully");
        });
        $scope.getMethods();
        $scope.editMode = false;
        $scope.customMethod = {
            name: "Please give name to this method",
            code: "Your code here",
            imports:"",
            dependencies:[]
        };
    };


    $scope.addCustomMethod = function () {
        console.log($scope.customMethod);
        $http.post('/auth/judgement', $scope.customMethod).success(function (data) {
            if(data.successful === false){
                $scope.newMethodOK = false;
                $rootScope.err = data;
                $location.url("/err");
            }else{
                console.log("Creating new method successful!");
                $scope.newMethodOK = true;
                $scope.getMethods();
            }
        }).error(function (data) {
            console.log("Creating new method failed");
            $scope.newMethodOK = false;
        });
        $scope.customMethod = {
            name: "Please give name to this method",
            code: "Your code here",
            imports:"",
            dependencies:[]
        };
    };
});
