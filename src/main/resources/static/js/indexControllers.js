var indexControllers = angular.module('indexControllers', []);
indexControllers.controller('navigationController',
    function ($rootScope, $scope, $http, $location) {
        $scope.credentials = {};
        $scope.login = function () {
            var cred = {
                name: $scope.credentials.username,
                password: $scope.credentials.password
            };
            $http.post('/rest/sign-in', cred).success(function (data, status, headers) {
                if (data.successful === false) {
                    console.log("Unsuccesful sign in");
                    $rootScope.err = data;
                    $location.url("/err");
                } else {
                    console.log("Sign in  ok");
                    $rootScope.authenticated = true;
                    $rootScope.loggedUser = cred.name;
                    console.log(headers());
                    if (data.messageCode == 222) {
                        $rootScope.admin = true;
                    } else if (data.messageCode == 223) {
                        $rootScope.superadmin = true;
                    }
                    $location.path('/myPolls');
                }
            }).error(function (data) {
                console.log("Unsuccesful sign in");
            });

        };
        $scope.logout = function () {
            $http.get('/auth/logout/').success(function () {
                $rootScope.authenticated = false;
                if (data.successful === false) {
                    console.log("Unsuccesful logout");
                    $rootScope.err = data;
                    $location.url("/err");
                } else {
                    console.log("Logout succeeded");
                    $location.path("/");
                }
            }).error(function () {
                $location.path("/");
                console.log("Logout failed");
            });
        };
    });

indexControllers.controller('newAccountController', function ($rootScope, $scope, $http) {
    $rootScope.NewAccountOK = false;
    $scope.setNewAcc = function () {
        var cred = {
            name: $scope.NewAccountEmail,
            password: $scope.NewAccountPassword
        };
        console.log("Creating new account...");
        console.log($scope.NewAccountEmail);
        $http.post('/rest/users', cred).success(function (data) {
            if (data.successful === false) {
                $rootScope.errorNewAccount = true;
                $rootScope.NewAccountOK = false;
                console.log("Unsuccesful creation of new account");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                console.log("Account has been created");
                $rootScope.errorNewAccount = false;
                $rootScope.NewAccountOK = true;
            }
        }).error(function (data) {
            console.log("Setting up account failed");
        });
    }
});
indexControllers.controller('errorController', function ($rootScope, $scope, $http) {
});

indexControllers.controller('jarsController', function ($rootScope, $scope, $http) {
    $scope.getLibraries = function () {
        $http.get('/auth/libraries').success(function (data) {
            if (data.successful === false) {
                console.log("Get libraries unsuccessfull");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                console.log(data);
                console.log("Get libraries successfull");
                $scope.libraries = data;
            }
        }).error(function (data) {
            console.log("Get libraries unsuccesfull");
        });
    };
    $scope.getLibraries();
    //file upload code below
    $scope.document = {};
    $scope.setTitle = function (fileInput) {
        var file = fileInput.value;
        var filename = file.replace(/^.*[\\\/]/, '');
        var title = filename.substr(0, filename.lastIndexOf('.'));
        $("#title").val(title);
        $("#title").focus();
        $scope.document.title = title;
    };
    $scope.uploadFile = function () {
        var formData = new FormData();
        formData.append("file", file.files[0]);
        $http({
            method: 'POST',
            url: '/auth/libraries',
            headers: {'Content-Type': undefined},
            data: formData
        }).success(function (data, status) {
                alert("File uploaded");
        }).error(function (data, status) {
                alert("Error!" + status);
        });
    };
});