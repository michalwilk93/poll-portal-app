var statisticsModule = angular.module('statisticsModule', []);
statisticsModule.controller('statisticsController', function ($rootScope, $scope) {
    console.log($rootScope.pollResult);
    $scope.data5 = $rootScope.pollResult.root;
    console.log($scope.data5);
});

statisticsModule.directive('treeChart', function ($rootScope) {
    return {
        restrict: 'EA',

        link: function (scope, elem, attrs) {
            var myjson = JSON.parse(JSON.stringify(scope.data5));
            console.log(myjson);
            var alternatives = $rootScope.currentPoll.alternatives;
            console.log(alternatives);

            var boxWidth = 200,
                boxHeight = 15 + alternatives.length * 15;

            var zoom = d3.behavior.zoom()
                .scaleExtent([.1, 1])
                .on('zoom', function () {
                    svg.attr("transform", "translate(" + d3.event.translate + ") scale(" + d3.event.scale + ")");
                })
                .translate([150, 200]);

            var svg = d3.select(elem[0]).append("svg")
                .attr('width', 1000)
                .attr('height', 400)
                .call(zoom)
                .append('g')
                .attr("transform", "translate(150,200)");

            var tree = d3.layout.tree()
                // Using nodeSize we are able to control
                // the separation between nodes. If we used
                // the size parameter instead then d3 would
                // calculate the separation dynamically to fill
                // the available space.
                .nodeSize([100 + alternatives.length * 35, 250])
                // By default, cousins are drawn further apart than siblings.
                // By returning the same value in all cases, we draw cousins
                // the same distance apart as siblings.
                .separation(function () {
                    return .5;
                })
                // Tell d3 what the child nodes are. Remember, we're drawing
                // a tree so the ancestors are child nodes.
                .children(function (crit) {
                    return crit.subcriteria;
                });

            var nodes = tree.nodes(myjson),
                links = tree.links(nodes);

            // Style links (edges)
            svg.selectAll("path.link")
                .data(links)
                .enter().append("path")
                .attr("class", "link")
                .attr("d", elbow);

            // Style nodes
            var node = svg.selectAll("g.person")
                .data(nodes)
                .enter().append("g")
                .attr("class", "person")
                .attr("transform", function (d) {
                    return "translate(" + d.y + "," + d.x + ")";
                });

            // Draw the rectangle person boxes
            node.append("rect")
                .attr({
                    x: -(boxWidth / 2),
                    y: -(boxHeight / 2),
                    width: boxWidth,
                    height: boxHeight
                });

            // Draw the person's name and position it inside the box
            node.append("text")
                .attr("dx", -(boxWidth / 2) + 10)
                .attr("dy", -(boxHeight / 2) + 10)
                .attr("text-anchor", "start")
                .attr('class', 'name')
                .text(function (d) {//funkcja bierze noda i zwraca stringa. ale jak tu dac nowa linie?
                    return d.name;
                });


            for (var i = 0; i < alternatives.length; i++) {
                node.append("text")
                    .attr("dx", -(boxWidth / 2) + 10)
                    .attr("dy", -(boxHeight / 2) + (25 + 15 * i))
                    .attr("text-anchor", "start")
                    .attr('class', 'name')
                    .text(function (d) {//funkcja bierze noda i zwraca stringa.(d to node a d.name to pole -nazwa kryterium)
                        //  ale jak tu dac nowa linie?
                        return alternatives[i] + ":   " + Math.round(d.vector[i] * 1000000) / 1000000;
                    });
            }

            /**
             * Custom path function that creates straight connecting lines.
             */
            function elbow(d) {
                return "M" + d.source.y + "," + d.source.x
                    + "H" + (d.source.y + (d.target.y - d.source.y) / 2)
                    + "V" + d.target.x
                    + "H" + d.target.y;
            }


        }
    }
});
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

statisticsModule.directive('linearChart', function () {
    return {
        restrict: 'EA',

        link: function (scope, elem, attrs) {
            var root = JSON.parse(JSON.stringify(scope.data5));
            console.log(root);
            var margin = {
                    top: 30,
                    right: 0,
                    bottom: 0,
                    left: 0
                },
                width = 400,
                height = 400 - margin.top - margin.bottom,
                formatNumber = d3.format(",%"),
                transitioning;

            var x = d3.scale.linear().domain([0, width]).range([0, width]);

            var y = d3.scale.linear().domain([0, height]).range([0, height]);

            var treemap = d3.layout.
                treemap()
                .children(function (d, depth) {
                    return depth ? null : d.subcriteria;//todo tu chyba subriteria
                })
                .sort(function (a, b) {
                    return a.value - b.value;
                })
                .ratio(height / width * 0.5 * (1 + Math.sqrt(5)))
                .round(false);


            var svg = d3.select(elem[0]).append("svg")
                .attr("width", width + margin.left + margin.right)
                .attr("height", height + margin.bottom + margin.top)
                .style("margin-left", -margin.left + "px")
                .style("margin.right", -margin.right + "px")
                .append("g")
                .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
                .style("shape-rendering", "crispEdges");

            var grandparent = svg.append("g")
                .attr("class", "grandparent");

            grandparent.append("rect")
                .attr("y", -margin.top)
                .attr("width", width)
                .attr("height", margin.top);

            grandparent.append("text")
                .attr("x", 6)
                .attr("y", 6 - margin.top)
                .attr("dy", ".75em");

            initialize(root);
            accumulate(root);
            layout(root);
            display(root);

            function initialize(root) {
                root.x = root.y = 0;
                root.dx = width;
                root.dy = height;
                root.depth = 0;
            }

            // Aggregate the values for internal nodes. This is normally done by the
            // treemap layout, but not here because of our custom implementation.
            // We also take a snapshot of the original children (_children) to avoid
            // the children being overwritten when when layout is computed.

            function accumulate(d) {
                return d.subcriteria
                    ? d.value = d.subcriteria.reduce(function (p, v) {
                    return p + accumulate(v);
                }, 0)
                    : d.value;
            }


            // Compute the treemap layout recursively such that each group of siblings
            // uses the same size (1×1) rather than the dimensions of the parent cell.
            // This optimizes the layout for the current zoom state. Note that a wrapper
            // object is created for the parent node for each group of siblings so that
            // the parent’s dimensions are not discarded as we recurse. Since each group
            // of sibling was laid out in 1×1, we must rescale to fit using absolute
            // coordinates. This lets us use a viewport to zoom.

            function layout(d) {
                if (d.subcriteria) {
                    treemap.nodes({subcriteria: d.subcriteria});
                    d.subcriteria.forEach(function (c) {//todo tu chyba subriteria
                        c.x = d.x + c.x * d.dx;
                        c.y = d.y + c.y * d.dy;
                        c.dx *= d.dx;
                        c.dy *= d.dy;
                        c.parent = d;
                        layout(c);
                    });
                }
            }

            function display(d) {
                grandparent
                    .datum(d.parent)
                    .on("click", transition)
                    .select("text")
                    .text(name(d));

                var g1 = svg.insert("g", ".grandparent")
                    .datum(d)
                    .attr("class", "depth");

                var g = g1.selectAll("g")
                    .data(d.subcriteria)
                    .enter().append("g");

                g.filter(function (d) {
                    return d.subcriteria;
                })
                    .classed("children", true)
                    .on("click", transition);

                g.selectAll(".child")
                    .data(function (d) {
                        return d.subcriteria || [d];
                    })
                    .enter().append("rect")
                    .attr("class", "child")
                    .call(rect);


                g.append("rect")
                    .attr("class", "parent")
                    .call(rect)
                    .append("title")
                    .text(function (d) {
                        return formatNumber(d.value);
                    });

                g.append("text")
                    .attr("dy", ".75em")
                    .text(function (d) {
                        return d.name;
                    })
                    .call(text);

                function transition(d) {
                    if (transitioning || !d) return;
                    transitioning = true;

                    var g2 = display(d),
                        t1 = g1.transition().duration(750),
                        t2 = g2.transition().duration(750);

                    // Update the domain only after entering new elements.
                    x.domain([d.x, d.x + d.dx]);
                    y.domain([d.y, d.y + d.dy]);

                    // Enable anti-aliasing during the transition.
                    svg.style("shape-rendering", null);

                    // Draw child nodes on top of parent nodes.
                    svg.selectAll(".depth").sort(function (a, b) {
                        return a.depth - b.depth;
                    });

                    // Fade-in entering text.
                    g2.selectAll("text").style("fill-opacity", 0);

                    // Transition to the new view.
                    t1.selectAll("text").call(text).style("fill-opacity", 0);
                    t2.selectAll("text").call(text).style("fill-opacity", 1);
                    t1.selectAll("rect").call(rect);
                    t2.selectAll("rect").call(rect);

                    // Remove the old node when the transition is finished.
                    t1.remove().each("end", function () {
                        svg.style("shape-rendering", "crispEdges");
                        transitioning = false;
                    });
                }

                return g;
            }

            function text(text) {
                text.attr("x", function (d) {
                    return x(d.x) + 6;
                }).attr("y", function (d) {
                    return y(d.y) + 6;
                });
            }

            function rect(rect) {
                rect.attr("x", function (d) {
                    return x(d.x);
                }).attr("y", function (d) {
                    return y(d.y);
                }).attr("width", function (d) {
                    return x(d.x + d.dx) - x(d.x);
                }).attr("height", function (d) {
                    return y(d.y + d.dy) - y(d.y);
                });
            }

            function size(d) {
                return d.value;
            }

            function name(d) {
                return d.parent ? name(d.parent) + "->" + d.name : d.name;
            }
        }
    };
});










