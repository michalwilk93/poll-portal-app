var pollApp = angular.module('pollApp', ['ngRoute', 'indexControllers', 'newPollControllerModule',
    'angularAwesomeSlider', 'pollControllers', 'pollTakingControllerModule', 'statisticsModule',
    'adminPanelModule', 'pollViewerModule','customJudgementMdl']);

pollApp.config(function ($routeProvider, $httpProvider) {
    $routeProvider.when('/', {
        //default template is login.html
        templateUrl: 'login.html',
        controller: 'navigationController'
    }).when('/newAccount', {
        templateUrl: 'newAccount.html',
        controller: 'newAccountController'
    }).when('/newPoll', {
        templateUrl: 'newPoll.html',
        controller: 'newPollController'
    }).when('/myPolls', {
        templateUrl: 'myPolls.html',
        controller: 'myPollsController'
    }).when('/pollTaking', {
        templateUrl: 'pollTaking.html',
        controller: 'pollTakingController'
    }).when('/statistics', {
        templateUrl: 'statistics.html',
        controller: 'statisticsController'
    }).when('/publicPolls', {
        templateUrl: 'publicPolls.html',
        controller: 'publicPollsController'
    }).when('/getPoll', {
        template: " ", //we don't need template for this controller
        controller: 'getPollFromLinkController'
    }).when('/adminpanel', {
        templateUrl: "adminView.html",
        controller: 'adminPanelController'
    }).when("/pollViewer", {
        templateUrl: "pollViewer.html",
        controller: 'pollViewerController'
    }).when("/err", {
        templateUrl: "err.html",
        controller: 'errorController'
    }).when("/customJudgement", {
        templateUrl: "customJudgement.html",
        controller: 'customJudgementController'
    }).when("/uploadedJars", {
        templateUrl: "uploadedJars.html",
        controller: 'jarsController'
    }).otherwise({
        redirectTo: '/'
    });
    $httpProvider.defaults.headers.common["X-Requested-With"] = 'XMLHttpRequest';
});


