var pollViewerModule = angular.module('pollViewerModule', []);

pollViewerModule.controller('pollViewerController', function ($rootScope, $scope, $http) {

    $scope.getPolls = function () {
        $scope.elementsInARow = 4;
        var sgIndex = 0;
        $scope.userPollsArrayGroups = [];
        $scope.userPollsArrayGroups[sgIndex] = [];
        $http.get("/auth/user-polls/" + $rootScope.seePolls).success(function (data) {
            if (data.successful === false) {
                console.log("Getting user-polls polls failed");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                $scope.pollsCreatedByUser = data;
                console.log("Getting user polls succeeded");
                for (var i = 0; i < $scope.pollsCreatedByUser.length; i++) {
                    var p = $scope.pollsCreatedByUser[i];
                    $scope.userPollsArrayGroups[sgIndex].push(p);
                    if ($scope.userPollsArrayGroups[sgIndex].length == $scope.elementsInARow) {
                        $scope.userPollsArrayGroups[++sgIndex] = [];
                    }
                }
            }
        }).error(function () {
            console.log("Cant get polls");
        });
    };
    $scope.getPolls();

    $scope.deletePoll = function (uuid) {
        $http.delete('/auth/polls/' + uuid).success(function (data) {
            if (data.successful === false) {
                console.log("Deleting poll failed");
                $rootScope.err = data;
                $location.url("/err");
            } else {
                console.log("Poll deleted");
                $scope.getPolls();
            }
        }).error(function () {
            console.log("Delete poll failed");
        });
    }
});
