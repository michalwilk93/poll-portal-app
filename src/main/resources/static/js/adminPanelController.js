var adminPanelModule = angular.module('adminPanelModule', []);
adminPanelModule.controller('adminPanelController', function ($rootScope, $scope, $http,$location) {
    console.log("ADMIN PANEL CONTROLLER");

    $scope.getUsers = function () {
        $http.get('/auth/users/').success(function (data) {
            if(data.successful === false){
                $rootScope.err = data;
                $location.url("/err");
            }else{
                $scope.userList = data;
                console.log(data)
            }
        }).error(function () {
            console.log("Get users failed");
        });
    };

    $scope.getUsers();

    $scope.changeStatus = function (name, status) {
        $http.get('/auth/changeStatus?user='+name+"&status="+status).success(function (data) {
            if(data.successful === false){
                $rootScope.err = data;
                $location.url("/err");
            }else{
                console.log("Status changed");
                $scope.getUsers();
            }
        }).error(function () {
            console.log("Change status failed");
        });
    };

    $scope.deleteUser = function (name) {
        console.log(name);
        $http.delete('/auth/users/' + name).success(function (data) {
            if(data.successful === false){
                $rootScope.err = data;
                $location.url("/err");
            }else{
                console.log("User deleted");
                $scope.getUsers();
            }

        }).error(function () {
            console.log("Delete user failed");
        });
    };

    $scope.seeUserPolls = function (name) {
        $rootScope.seePolls = name;
        $location.path("/pollViewer");
    };
});